#ifndef __TOOL__H
#define __TOOL__H
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using std::vector;
using std::endl;
using std::cout;
using std::string;
using std::ifstream;
template <typename T> void readfile(const string inf,vector<T>& inv){
    ifstream file1(inf);
    if(!file1.good()){
        cout<<"file bad"<<endl;
        return;
    }
    T tmp;
    while(file1>>tmp){
        inv.push_back(tmp);
        cout<<tmp<<endl;
    }
    file1.close();
}
#endif
