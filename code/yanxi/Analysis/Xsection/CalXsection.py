from ROOT import *
from math import atan,exp,sqrt
import sys
sys.path +=["/afs/cern.ch/user/z/zhangy/workdir/psitwos_pp_2012/ScriptsJiangFeng/code/yanxi/Analysis"]
from formats import colors,markers
from array import array

gROOT.ProcessLine(".x ~/lhcbStyle.C")

BF1 = 0.05961
BF2 = 0.00793
ybins  = [2.0+ii*.5 for ii in range(6)]
ptbins = range(9) + [10, 14, 20]
ptbins = [ii*1000. for ii in ptbins]

tmp=array('d',ptbins)

histos={}

fyields = TFile("../SignalYield/yields.root")
histos["NJpsi"]=[fyields.Get("NJpsi"+str(ii)) for ii in range(5)]
histos["NPsi2S"]=[fyields.Get("Npsi2S"+str(ii)) for ii in range(5)]

fFB = TFile("../SignalYield/fraction_fromB.root")
histos["FBJpsi"]=[fFB.Get("FBJpsi"+str(ii)) for ii in range(5)]
histos["FBPsi2S"]=[fFB.Get("FBpsi2S"+str(ii)) for ii in range(5)]
histos["FPJpsi"]=[TH1F("FP_Jpsi"+str(ii),"",10,0.,1) for ii in range(5)]
histos["FPPsi2S"]=[TH1F("FP_Psi2S"+str(ii),"",10,0.,1) for ii in range(5)]

for hh in histos["FPJpsi"]+histos["FPPsi2S"]:
    hh.SetBins(11,tmp)
for ii in range(5):
    for jj in range(11):
        histos["FPJpsi"][ii].SetBinContent(jj+1,1.-histos["FBJpsi"][ii].GetBinContent(jj+1));
        histos["FPJpsi"][ii].SetBinError(jj+1,histos["FBJpsi"][ii].GetBinError(jj+1));
        histos["FPPsi2S"][ii].SetBinContent(jj+1,1.-histos["FBPsi2S"][ii].GetBinContent(jj+1));
        histos["FPPsi2S"][ii].SetBinError(jj+1,histos["FBPsi2S"][ii].GetBinError(jj+1));



fAcc1=TFile("../Efficiency/acceptance/AccEffPsi2S.root")
histos["AccPsi2S"]=[fAcc1.Get("hacc"+str(ii)) for ii in range(5)]
fAcc2=TFile("../Efficiency/acceptance/AccEffJpsi.root")
histos["AccJpsi"]=[fAcc2.Get("hacc"+str(ii)) for ii in range(5)]


fRec1=TFile("../Efficiency/reconseff/RecEffPsi2S.root")
histos["RecPsi2S"]=[fRec1.Get("hrec"+str(ii)) for ii in range(5)]
fRec2=TFile("../Efficiency/reconseff/RecEffJpsi.root")
histos["RecJpsi"]=[fRec2.Get("hrec"+str(ii)) for ii in range(5)]


fTrg1=TFile("../Efficiency/reconseff/TrgEffPsi2S.root")
histos["TrgPsi2S"]=[fTrg1.Get("htrg"+str(ii)) for ii in range(5)]
fTrg2=TFile("../Efficiency/reconseff/TrgEffJpsi.root")
histos["TrgJpsi"]=[fTrg2.Get("htrg"+str(ii)) for ii in range(5)]


fPid1=TFile("../Efficiency/pideff/PIDEffPsi2S.root")
histos["PidPsi2S"]=[fPid1.Get("hpid"+str(ii)) for ii in range(5)]
fPid2=TFile("../Efficiency/pideff/PIDEffJpsi.root")
histos["PidJpsi"]=[fPid2.Get("hpid"+str(ii)) for ii in range(5)]



hxB_Jpsi = [TH1F("hxB_Jpsi"+str(ii),"",10,0.,1) for ii in range(5)]
hxB_Psi2S =[TH1F("hxB_Psi2S"+str(ii),"",10,0.,1) for ii in range(5)]
hxB_Ratio =[TH1F("hxB_Ratio"+str(ii),"",10,0.,1) for ii in range(5)]
hxP_Jpsi = [TH1F("hxP_Jpsi"+str(ii),"",10,0.,1) for ii in range(5)]
hxP_Psi2S =[TH1F("hxP_Psi2S"+str(ii),"",10,0.,1) for ii in range(5)]
hxP_Ratio =[TH1F("hxP_Ratio"+str(ii),"",10,0.,1) for ii in range(5)]

hxB_Jpsi_pt1D = TH1F("hxB_Jpsi_pt1D","",10,0.,1) 
hxB_Psi2S_pt1D = TH1F("hxB_Psi2S_pt1D","",10,0.,1) 
hxB_Ratio_pt1D = TH1F("hxB_ratio_pt1D","",10,0.,1) 

hxP_Jpsi_pt1D = TH1F("hxP_Jpsi_pt1D","",10,0.,1) 
hxP_Psi2S_pt1D = TH1F("hxP_Psi2S_pt1D","",10,0.,1) 
hxP_Ratio_pt1D = TH1F("hxP_ratio_pt1D","",10,0.,1) 

for hh in hxB_Jpsi+hxB_Psi2S+hxB_Ratio:
    hh.SetBins(11,tmp)
    hh.Sumw2()
for hh in hxP_Jpsi+hxP_Psi2S+hxP_Ratio:
    hh.SetBins(11,tmp)
    hh.Sumw2()
for hh in [hxB_Jpsi_pt1D,hxB_Psi2S_pt1D,hxB_Ratio_pt1D,hxP_Jpsi_pt1D,hxP_Psi2S_pt1D,hxP_Ratio_pt1D]:
    hh.SetBins(11,tmp)
    hh.Sumw2()

hxB_Jpsi_y1D = TH1F("hxB_Jpsi_y1D","",5,2,4.5) 
hxB_Psi2S_y1D = TH1F("hxB_Psi2S_y1D","",5,2,4.5) 
hxB_Ratio_y1D = TH1F("hxB_ratio_y1D","",5,2,4.5) 

hxP_Jpsi_y1D = TH1F("hxP_Jpsi_y1D","",5,2,4.5) 
hxP_Psi2S_y1D = TH1F("hxP_Psi2S_y1D","",5,2,4.5) 
hxP_Ratio_y1D = TH1F("hxP_ratio_y1D","",5,2,4.5) 

for ii in range(5):
    hxB_Jpsi[ii].Multiply(histos["NJpsi"][ii],histos["FBJpsi"][ii])
    hxB_Jpsi[ii].Divide(hxB_Jpsi[ii],histos["AccJpsi"][ii],1, BF1)
    hxB_Jpsi[ii].Divide(hxB_Jpsi[ii],histos["RecJpsi"][ii])
    hxB_Jpsi[ii].Divide(hxB_Jpsi[ii],histos["TrgJpsi"][ii])
    hxB_Jpsi[ii].Divide(hxB_Jpsi[ii],histos["PidJpsi"][ii])
    #print histos["PidJpsi"][ii].GetXaxis().GetNbins()

    hxB_Psi2S[ii].Multiply(histos["NPsi2S"][ii],histos["FBPsi2S"][ii])
    hxB_Psi2S[ii].Divide(hxB_Psi2S[ii],histos["AccPsi2S"][ii],1, BF2)
    hxB_Psi2S[ii].Divide(hxB_Psi2S[ii],histos["RecPsi2S"][ii])
    hxB_Psi2S[ii].Divide(hxB_Psi2S[ii],histos["TrgPsi2S"][ii])
    hxB_Psi2S[ii].Divide(hxB_Psi2S[ii],histos["PidPsi2S"][ii])


    hxP_Jpsi[ii].Multiply(histos["NJpsi"][ii],histos["FPJpsi"][ii])
    hxP_Jpsi[ii].Divide(hxP_Jpsi[ii],histos["AccJpsi"][ii],1, BF1)
    hxP_Jpsi[ii].Divide(hxP_Jpsi[ii],histos["RecJpsi"][ii])
    hxP_Jpsi[ii].Divide(hxP_Jpsi[ii],histos["TrgJpsi"][ii])
    hxP_Jpsi[ii].Divide(hxP_Jpsi[ii],histos["PidJpsi"][ii])

    hxP_Psi2S[ii].Multiply(histos["NPsi2S"][ii],histos["FPPsi2S"][ii])
    hxP_Psi2S[ii].Divide(hxP_Psi2S[ii],histos["AccPsi2S"][ii],1, BF2)
    hxP_Psi2S[ii].Divide(hxP_Psi2S[ii],histos["RecPsi2S"][ii])
    hxP_Psi2S[ii].Divide(hxP_Psi2S[ii],histos["TrgPsi2S"][ii])
    hxP_Psi2S[ii].Divide(hxP_Psi2S[ii],histos["PidPsi2S"][ii])

    hxB_Ratio[ii].Divide(hxB_Psi2S[ii],hxB_Jpsi[ii])
    hxP_Ratio[ii].Divide(hxP_Psi2S[ii],hxP_Jpsi[ii])

def Inte_pt1D(a,b):
    for ii in range(11):
        vv,ee = 0., 0.
        for jj in range(5):
            vv += (b[jj].GetBinContent(ii+1))
            ee += pow(b[jj].GetBinError(ii+1),2.)
        a.SetBinContent(ii+1,vv)
        a.SetBinError(ii+1,sqrt(ee))

Inte_pt1D(hxB_Jpsi_pt1D,hxB_Jpsi)
Inte_pt1D(hxP_Jpsi_pt1D,hxP_Jpsi)
Inte_pt1D(hxB_Psi2S_pt1D,hxB_Psi2S)
Inte_pt1D(hxP_Psi2S_pt1D,hxP_Psi2S)

hxP_Ratio_pt1D.Divide(hxP_Psi2S_pt1D,hxP_Jpsi_pt1D)
hxB_Ratio_pt1D.Divide(hxB_Psi2S_pt1D,hxB_Jpsi_pt1D)

def Inte_y1D(a,b):
    for jj in range(5):
        vv,ee = 0., 0.
        for ii in range(11):
            vv += (b[jj].GetBinContent(ii+1))
            ee += pow(b[jj].GetBinError(ii+1),2.)
        a.SetBinContent(jj+1,vv)
        a.SetBinError(jj+1,sqrt(ee))

Inte_y1D(hxB_Jpsi_y1D,hxB_Jpsi)
Inte_y1D(hxP_Jpsi_y1D,hxP_Jpsi)
Inte_y1D(hxB_Psi2S_y1D,hxB_Psi2S)
Inte_y1D(hxP_Psi2S_y1D,hxP_Psi2S)

hxP_Ratio_y1D.Divide(hxP_Psi2S_y1D,hxP_Jpsi_y1D)
hxB_Ratio_y1D.Divide(hxB_Psi2S_y1D,hxB_Jpsi_y1D)

def Inte2D(b):
    vv,ee = 0.,0.
    for ii in range(11):
        for jj in range(5):
            vv += (b[jj].GetBinContent(ii+1))
            ee += pow(b[jj].GetBinError(ii+1),2.)
    return vv,sqrt(ee)

xB_Jpsi_total = Inte2D(hxB_Jpsi)
xP_Jpsi_total = Inte2D(hxP_Jpsi)
xB_Psi2S_total = Inte2D(hxB_Psi2S)
xP_Psi2S_total = Inte2D(hxP_Psi2S)

rB,eB = xB_Psi2S_total[0]/xB_Jpsi_total[0],xB_Psi2S_total[1]/xB_Jpsi_total[0]
rP,eP = xP_Psi2S_total[0]/xP_Jpsi_total[0],xP_Psi2S_total[1]/xP_Jpsi_total[0]

#sys.exit()


can=TCanvas("can","",600,500)
leg=TLegend(0.6,0.2,0.8,0.35)
leg.SetTextSize(0.03)
leg.SetTextFont(132)
leg.AddEntry(hxP_Jpsi[0],"2.0 < #it{y} < 2.5","lpe")
leg.AddEntry(hxP_Jpsi[1],"2.5 < #it{y} < 3.0","lpe")
leg.AddEntry(hxP_Jpsi[2],"3.0 < #it{y} < 3.5","lpe")
leg.AddEntry(hxP_Jpsi[3],"3.5 < #it{y} < 3.5","lpe")
leg.AddEntry(hxP_Jpsi[4],"4.0 < #it{y} < 4.5","lpe")
for ii in range(5):
    hxP_Jpsi[ii].SetLineColor(colors[ii])
    hxP_Jpsi[ii].SetMarkerColor(colors[ii])
    hxP_Jpsi[ii].SetMarkerStyle(markers[ii])
hxP_Jpsi[0].Draw("e")
hxP_Jpsi[0].SetMinimum(0.)
hxP_Jpsi[0].SetXTitle("p_{T}")
#hxP_Jpsi[0].SetMaximum(1.)
for ii in range(1,5):
    hxP_Jpsi[ii].Draw("same,e")
leg.Draw("same")
can.SaveAs("XSectionJpsiPrompt.pdf")


can=TCanvas("can","",600,500)
for ii in range(5):
    hxB_Jpsi[ii].SetLineColor(colors[ii])
    hxB_Jpsi[ii].SetMarkerColor(colors[ii])
    hxB_Jpsi[ii].SetMarkerStyle(markers[ii])
hxB_Jpsi[0].Draw("e")
hxB_Jpsi[0].SetXTitle("p_{T}")
hxB_Jpsi[0].SetMinimum(0.)
#hxB_Jpsi[0].SetMaximum(1.)
for ii in range(1,5):
    hxB_Jpsi[ii].Draw("same,e")
leg.Draw("same")
can.SaveAs("XSectionJpsiFromB.pdf")

can=TCanvas("can","",600,500)
for ii in range(5):
    hxB_Psi2S[ii].SetLineColor(colors[ii])
    hxB_Psi2S[ii].SetMarkerColor(colors[ii])
    hxB_Psi2S[ii].SetMarkerStyle(markers[ii])
hxB_Psi2S[0].Draw("e")
hxB_Psi2S[0].SetXTitle("p_{T}")
hxB_Psi2S[0].SetMinimum(0.)
#hxB_Psi2S[0].SetMaximum(1.)
for ii in range(1,5):
    hxB_Psi2S[ii].Draw("same,e")
leg.Draw("same")
can.SaveAs("XSectionPsi2SFromB.pdf")


can=TCanvas("can","",600,500)
for ii in range(5):
    hxP_Psi2S[ii].SetLineColor(colors[ii])
    hxP_Psi2S[ii].SetMarkerColor(colors[ii])
    hxP_Psi2S[ii].SetMarkerStyle(markers[ii])
hxP_Psi2S[0].Draw("e")
hxP_Psi2S[0].SetXTitle("p_{T}")
hxP_Psi2S[0].SetMinimum(0.)
#hxP_Psi2S[0].SetMaximum(1.)
for ii in range(1,5):
    hxP_Psi2S[ii].Draw("same,e")
leg.Draw("same")
can.SaveAs("XSectionPsi2SPrompt.pdf")


can=TCanvas("can","",600,500)
for ii in range(5):
    hxP_Ratio[ii].SetLineColor(colors[ii])
    hxP_Ratio[ii].SetMarkerColor(colors[ii])
    hxP_Ratio[ii].SetMarkerStyle(markers[ii])
hxP_Ratio[0].Draw("e")
hxP_Ratio[0].SetMinimum(0.)
hxP_Ratio[0].SetXTitle("p_{T}")
#hxP_Ratio[0].SetMaximum(1.)
for ii in range(1,5):
    hxP_Ratio[ii].Draw("same,e")
leg.Draw("same")
can.SaveAs("XSectionRatioPrompt.pdf")


can=TCanvas("can","",600,500)
for ii in range(5):
    hxB_Ratio[ii].SetLineColor(colors[ii])
    hxB_Ratio[ii].SetMarkerColor(colors[ii])
    hxB_Ratio[ii].SetMarkerStyle(markers[ii])
hxB_Ratio[0].Draw("e")
hxB_Ratio[0].SetXTitle("p_{T}")
hxB_Ratio[0].SetMinimum(0.)
#hxB_Ratio[0].SetMaximum(1.)
for ii in range(1,5):
    hxB_Ratio[ii].Draw("same,e")
leg.Draw("same")
can.SaveAs("XSectionRatioFromB.pdf")

#####_pt1D
can=TCanvas("can","",600,500)
hxP_Jpsi_pt1D.Draw()
hxP_Jpsi_pt1D.SetXTitle("p_{T}")
can.SaveAs("XSectionJpsi_pt1DPrompt.pdf")

can=TCanvas("can","",600,500)
hxB_Jpsi_pt1D.Draw()
hxB_Jpsi_pt1D.SetXTitle("p_{T}")
can.SaveAs("XSectionJpsi_pt1DFromB.pdf")


can=TCanvas("can","",600,500)
hxP_Psi2S_pt1D.Draw()
hxP_Psi2S_pt1D.SetXTitle("p_{T}")
can.SaveAs("XSectionPsi2S_pt1DPrompt.pdf")

can=TCanvas("can","",600,500)
hxB_Psi2S_pt1D.Draw()
hxB_Psi2S_pt1D.SetXTitle("p_{T}")
can.SaveAs("XSectionPsi2S_pt1DFromB.pdf")

hrp_old=TH1F("hrp_old","",10,0.,1.)
hrp_old.SetBins(10,tmp)
hrp_old.SetLineColor(kRed)
hrp_old.SetMarkerColor(kRed)
rlt= [0.1057,0.1300,0.1452,0.1764,0.1958,0.2176,0.2612,0.2584,0.2801,0.3661]
rlte=[0.0176,0.0181,0.0117,0.0111,0.0123,0.0125,0.0150,0.0157,0.0267,0.0424]
for ii in range(10):
    hrp_old.SetBinContent(ii+1,rlt[ii])
    hrp_old.SetBinError(ii+1,rlte[ii])

can=TCanvas("can","",600,500)
hxP_Ratio_pt1D.Draw()
hxP_Ratio_pt1D.SetXTitle("p_{T}")
hxP_Ratio_pt1D.SetMinimum(0.)
hrp_old.Draw("same,e")
can.SaveAs("XSectionRatio_pt1DPrompt.pdf")

hrb_old=TH1F("hrb_old","",10,0.,1.)
hrb_old.SetBins(10,tmp)
hrb_old.SetLineColor(kRed)
hrb_old.SetMarkerColor(kRed)
rlt= [0.1524,0.2338,0.2165,0.2327,0.2903,0.2958,0.2759,0.2897,0.2936,0.3312]
rlte=[0.0199,0.0188,0.0144,0.0120,0.0132,0.0163,0.0169,0.0292,0.0252,0.0419]
for ii in range(10):
    hrb_old.SetBinContent(ii+1,rlt[ii])
    hrb_old.SetBinError(ii+1,rlte[ii])
can=TCanvas("can","",600,500)
hxB_Ratio_pt1D.Draw()
hxB_Ratio_pt1D.SetXTitle("p_{T}")
hxB_Ratio_pt1D.SetMinimum(0.)
hrb_old.Draw("same,e")
can.SaveAs("XSectionRatio_pt1DFromB.pdf")

#####_y1D
can=TCanvas("can","",600,500)
hxP_Jpsi_y1D.Draw()
hxP_Jpsi_y1D.SetXTitle("y")
hxP_Jpsi_y1D.SetMinimum(0.)
can.SaveAs("XSectionJpsi_y1DPrompt.pdf")

can=TCanvas("can","",600,500)
hxB_Jpsi_y1D.Draw()
hxB_Jpsi_y1D.SetXTitle("y")
hxB_Jpsi_y1D.SetMinimum(0.)
can.SaveAs("XSectionJpsi_y1DFromB.pdf")

can=TCanvas("can","",600,500)
hxP_Psi2S_y1D.Draw()
hxP_Psi2S_y1D.SetXTitle("y")
hxP_Psi2S_y1D.SetMinimum(0.)
can.SaveAs("XSectionPsi2S_y1DPrompt.pdf")

can=TCanvas("can","",600,500)
hxB_Psi2S_y1D.Draw("")
hxB_Psi2S_y1D.SetXTitle("y")
hxB_Psi2S_y1D.SetMinimum(0.)
can.SaveAs("XSectionPsi2S_y1DFromB.pdf")


can=TCanvas("can","",600,500)
hxP_Ratio_y1D.Draw()
hxP_Ratio_y1D.SetXTitle("y")
hxP_Ratio_y1D.Fit("pol0")
hxP_Ratio_y1D.SetMinimum(0.05)
hxP_Ratio_y1D.SetMaximum(0.20)
box=TBox()
box.SetLineColor(kBlue)
box.SetFillColor(kRed+2)
box.SetFillStyle(3844)
box.DrawBox(2., 0.152-0.013, 4.5,0.152+0.013)
lt=TLatex()
lt.DrawLatex(2.5,0.152,"old, prompt")
can.SaveAs("XSectionRatio_y1DPrompt.pdf")

can=TCanvas("can","",600,500)
hxB_Ratio_y1D.Draw("same")
hxB_Ratio_y1D.SetXTitle("y")
hxB_Ratio_y1D.SetMinimum(0.0)
hxB_Ratio_y1D.SetMaximum(0.5)
hxB_Ratio_y1D.Fit("pol0")
box.DrawBox(2., 0.231-0.025, 4.5,0.231+0.025)
lt=TLatex()
lt.DrawLatex(2.5,0.152,"old, fromB")
can.SaveAs("XSectionRatio_y1DFromB.pdf")



fw=TFile("XSection.root","recreate")
for h in hxB_Jpsi+hxB_Psi2S+hxB_Jpsi+hxB_Jpsi+hxP_Ratio+hxB_Ratio:
    h.Write()
for h in [hxB_Jpsi_pt1D,hxB_Psi2S_pt1D,hxB_Jpsi_pt1D,hxB_Jpsi_pt1D,hxP_Ratio_pt1D,hxB_Ratio_pt1D]:
    h.Write()
print "%2.4f+/-%2.4f      %2.4f+/-%2.4f"%(rB,eB,rP,eP)
#0.2581+/-0.0071      0.1349+/-0.0018

