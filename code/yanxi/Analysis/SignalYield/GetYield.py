from ROOT import *
from math import sqrt

gROOT.ProcessLine(".x ~/lhcbStyle.C")



def GetData(fr,name):
    myData=open(fr)
    #Nsig =  1243.5 +/- 86.823 L(0 - 68697.6) 
    #n1 =  1.0000 C L(-INF - +INF)
    for ln in myData.readlines():
        if name in ln:
            field = ln.split()
            if 'INF' in ln: return float(filed[2])
            else: return [float(field[2]), float(field[4])]




#print GetData("../../Fit/txt/PsiTwosDYLight_2_1Mass1D.txt","Nsig")

rootname = "/afs/cern.ch/user/z/zhangy/workdir/psitwos_pp_2012/ScriptsJiangFeng/code/yanxi/"
loc1 = rootname+"Fit/txt/Psi2SDY_Jpsi_{0[yy]}_{0[pt1]}_{0[pt2]}Mass.txt"
loc2 = rootname+"Fit/txt/Psi2SDiMuon_Jpsi_{0[yy]}_{0[pt1]}_{0[pt2]}Mass.txt"
#h5R = [TH2F("hN1"+str(ii),"",20,0+ii*0.2,20+ii*0.2) for ii in range(5)]
h5R = [TH1F("Npsi2S"+str(ii),"",20,0,20) for ii in range(5)]
g5R = [TH1F("NJpsi"+str(ii),"",10,0,20) for ii in range(5)]

from array import array
bins=array("d",range(9)+[10., 14., 20.])
ptbins = array("d",[ii*1000. for ii in bins])


for hh in h5R: hh.SetBins(11,ptbins)
for gg in g5R: gg.SetBins(11,ptbins)
hA = TH1F("hA","",20, -4,4)
lf1 = 4 #lumi_factor
lf2 = 1./5 #lumi_factor
for ii in range(5):
    for jj in range(11):
        nn1,ee1 = 0., 0.
        nn2,ee2 = 0., 0.
        if jj<2: 
            nn1,ee1 = GetData(loc1.format({"yy":ii,"pt1":int(bins[jj]+0.1), "pt2":int(0.1+bins[jj+1])}), "NsigA")
            nn2,ee2 = GetData(loc1.format({"yy":ii,"pt1":int(bins[jj]+0.1), "pt2":int(0.1+bins[jj+1])}), "NsigB")
            nn1 *=lf1
            ee1 *=lf1
        if jj>1: 
            nn1,ee1 = GetData(loc2.format({"yy":ii,"pt1":int(bins[jj]+0.1), "pt2":int(0.1+bins[jj+1])}), "NsigA")
            nn2,ee2 = GetData(loc2.format({"yy":ii,"pt1":int(bins[jj]+0.1), "pt2":int(0.1+bins[jj+1])}), "NsigB")
            nn1 *=lf2
            ee1 *=lf2
        h5R[ii].SetBinContent(jj+1, nn1)
        h5R[ii].SetBinError(jj+1  , ee1)
        g5R[ii].SetBinContent(jj+1, nn2)
        g5R[ii].SetBinError(jj+1  , ee2)

colors=[kBlack,kRed,kGreen+2,kBlue+2,kCyan+2]
fs=TFile("yields.root","recreate")
for ii in range(5):
    h5R[ii].Write()
    g5R[ii].Write()
    h5R[ii].SetLineColor(colors[ii])
    g5R[ii].SetLineColor(colors[ii])
    h5R[ii].SetMarkerColor(colors[ii])
    g5R[ii].SetMarkerColor(colors[ii])
    h5R[ii].SetMarkerStyle(22+ii*2)
    g5R[ii].SetMarkerStyle(22+ii*2)


can=TCanvas("can","",600,500)
h5R[0].Draw("e")
h5R[0].SetMinimum(0.)
h5R[0].SetMaximum(h5R[0].GetMaximum()*4.)
h5R[0].SetXTitle("#it{p}_{T}(#psi(2S))")
h5R[0].SetYTitle("#it{y}(#psi(2S))")
lt=TLatex()
lt.SetNDC()
lt.SetTextFont(132)
lt.SetTextSize(0.05)
lt.DrawLatex(0.75,0.85, "#psi, 2012")
lt.DrawLatex(0.55,0.70, "Statistical uncertainty")
for ii in range(1,5):
    h5R[ii].Draw("same,e")
can.SaveAs("Psi2SYields_Inclusive.pdf")

cam=TCanvas("cam","",600,500)
g5R[0].Draw("e")
g5R[0].SetXTitle("#it{p}_{T}(#psi(2S))")
g5R[0].SetYTitle("#it{y}(#psi(2S))")
g5R[0].SetMinimum(0.)
g5R[0].SetMaximum(g5R[0].GetMaximum()*4.)
lt=TLatex()
lt.SetNDC()
lt.SetTextFont(132)
lt.SetTextSize(0.05)
lt.DrawLatex(0.75,0.85, "#psi, 2012")
lt.DrawLatex(0.55,0.70, "Statistical uncertainty")
for ii in range(1,5):
    g5R[ii].Draw("same,e")
cam.SaveAs("JpsiYields_Inclusive.pdf")

