from ROOT import *
from math import sqrt

gROOT.ProcessLine(".x ~/lhcbStyle.C")



def GetData(fr,name):
    myData=open(fr)
    #Nsig =  1243.5 +/- 86.823 L(0 - 68697.6) 
    #n1 =  1.0000 C L(-INF - +INF)
    for ln in myData.readlines():
        if name in ln:
            field = ln.split()
            if 'INF' in ln: return float(filed[2])
            else: return [float(field[2]), float(field[4])]




#print GetData("../../Fit/txt/PsiTwosDYLight_2_1Mass1D.txt","Nsig")

rootname = "/afs/cern.ch/user/z/zhangy/workdir/psitwos_pp_2012/"
loc1 = rootname+"Fit/txt/Psi2SDY_Psi2SDiMuon_{0[yy]}_{0[pt1]}_{0[pt2]}Mass.txt"
loc2 = rootname+"Fit/txt/Psi2SDY_Jpsi_{0[yy]}_{0[pt1]}_{0[pt2]}Mass.txt"
h5R = [TH1F("hr1"+str(ii),"",15,0+ii*0.2,15+ii*0.2) for ii in range(5)]

from array import array

bins=array("d",range(9)+[10., 14., 20.])
for hh in h5R:
    hh.SetBins(11,bins)

hA = TH1F("hA","",20, -4,4)

ge = TGraphErrors()

for ii in range(5):
    for jj in range(11):
        if jj>1: nn1,ee1 = GetData(loc1.format({"yy":ii,"pt1":int(bins[jj]+0.1), "pt2":int(0.1+bins[jj+1])}), "fsig")
        if jj>1:
            h5R[ii].SetBinContent(jj+1, nn1)
            h5R[ii].SetBinError(jj+1, ee1)
            if jj<10: hA.Fill((nn1-20)/ee1)
            ge.SetPoint(ii*11+jj,(bins[jj]+bins[jj+1])/2., nn1)
            ge.SetPointError(ii*11+jj,(-bins[jj]+bins[jj+1])/2., ee1)




can=TCanvas("can","",600,500)
h5R[0].Draw("e")
h5R[0].SetMinimum(0.)
h5R[0].SetMaximum(40.)
h5R[0].SetXTitle("index (#it{p}_{T})")
h5R[0].SetYTitle("R^{#psi(2S)}_{DiMuon/DY}")
colors=[kRed,kGreen+2,kBlue+2,kCyan+2]
lt=TLatex()
lt.SetNDC()
lt.SetTextFont(132)
lt.SetTextSize(0.05)
lt.DrawLatex(0.25,0.85, "#psi(2S), 2012")
lt.DrawLatex(0.25,0.78, "Color: index(#it{y})")
lt.DrawLatex(0.25,0.30, "Statistical uncertainty")
h5R[0].Fit("pol1")
for ii in range(1,5):
    color=colors[ii-1]
    h5R[ii].SetLineColor(color)
    h5R[ii].Fit("pol1")
    h5R[ii].SetMarkerColor(color)
    h5R[ii].SetMarkerStyle(22+ii*2)
    h5R[ii].Draw("same,e")
ln=TLine()
ln.SetLineStyle(kDashed)
ln.SetLineColor(kYellow+2)
ln.DrawLine(0.,20.,15.,20)
can.SaveAs("DY_over_DiMuon_simFit.pdf")

ge.Fit("pol0")

cap=TCanvas("cap","",600,500)
hA.Draw("e")
hA.SetXTitle("Pull(DY/DiMuon)")
hA.Fit("gaus")
cap.SaveAs("NomarlizedDiffPsi2S_simFit.pdf")
