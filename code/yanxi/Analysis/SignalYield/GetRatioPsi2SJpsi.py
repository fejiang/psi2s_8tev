from ROOT import *
from math import sqrt

gROOT.ProcessLine(".x ~/lhcbStyle.C")



def GetData(fr,name):
    myData=open(fr)
    #Nsig =  1243.5 +/- 86.823 L(0 - 68697.6) 
    #n1 =  1.0000 C L(-INF - +INF)
    for ln in myData.readlines():
        if name in ln:
            field = ln.split()
            if 'INF' in ln: return float(filed[2])
            else: return [float(field[2]), float(field[4])]




#print GetData("../../Fit/txt/PsiTwosDYLight_2_1Mass1D.txt","Nsig")

rootname = "/afs/cern.ch/user/z/zhangy/workdir/psitwos_pp_2012/"
loc1 = rootname+"Fit/txt/Psi2SDY_Jpsi_{0[yy]}_{0[pt1]}_{0[pt2]}Mass.txt"
loc2 = rootname+"Fit/txt/Psi2SDiMuon_Jpsi_{0[yy]}_{0[pt1]}_{0[pt2]}Mass.txt"
h5R = [TH1F("hr1"+str(ii),"",20,0+ii*0.2,20+ii*0.2) for ii in range(5)]
h5R = [TH1F("hr1"+str(ii),"",20,0,20) for ii in range(5)]
g5R = [TH1F("hr2"+str(ii),"",5,2,4.5) for ii in range(11)]

from array import array
bins=array("d",range(9)+[10., 14., 20.])
for hh in h5R:
    hh.SetBins(11,bins)
hA = TH1F("hA","",20, -4,4)
lf1 = 4 #lumi_factor
lf2 = 1./5 #lumi_factor
for jj in range(11):
    print "\n"
    for ii in range(5):
        nn1,ee1 = 0., 0.
        nn2,ee2 = 0., 0.
        rr ,ee  = 0., 0.
        if jj<2: 
            nn1,ee1 = GetData(loc1.format({"yy":ii,"pt1":int(bins[jj]+0.1), "pt2":int(0.1+bins[jj+1])}), "NsigA")
            nn2,ee2 = GetData(loc1.format({"yy":ii,"pt1":int(bins[jj]+0.1), "pt2":int(0.1+bins[jj+1])}), "NsigB")
            rr = nn1/nn2 * lf1
            ee = rr * sqrt(pow(ee1/nn1,2.)+pow(ee2/nn2,2.)) 
            print nn1,ee1,nn2,ee2,rr,ee
        if jj>1: 
            nn1,ee1 = GetData(loc2.format({"yy":ii,"pt1":int(bins[jj]+0.1), "pt2":int(0.1+bins[jj+1])}), "NsigA")
            nn2,ee2 = GetData(loc2.format({"yy":ii,"pt1":int(bins[jj]+0.1), "pt2":int(0.1+bins[jj+1])}), "NsigB")
            rr = nn1/nn2 * lf2
            ee = rr * sqrt(pow(ee1/nn1,2.)+pow(ee2/nn2,2.))
        h5R[ii].SetBinContent(jj+1, rr)
        h5R[ii].SetBinError(jj+1  , ee)
        g5R[jj].SetBinContent(ii+1, rr)
        g5R[jj].SetBinError(ii+1  , ee)
        print rr,ee


for hh in g5R:
    hh.Fit("pol0")

can=TCanvas("can","",600,500)
h5R[0].Draw("e")
h5R[0].SetMinimum(0.)
h5R[0].SetMaximum(0.08)
h5R[0].SetXTitle("index (#it{p}_{T})")
h5R[0].SetYTitle("R_{#psi(2S)/J/#psi}")
colors=[kRed,kGreen+2,kBlue+2,kCyan+2]
lt=TLatex()
lt.SetNDC()
lt.SetTextFont(132)
lt.SetTextSize(0.05)
lt.DrawLatex(0.25,0.85, "#psi, 2012")
lt.DrawLatex(0.25,0.78, "Color: index(#it{y})")
lt.DrawLatex(0.25,0.30, "Statistical uncertainty")
for ii in range(1,5):
    color=colors[ii-1]
    h5R[ii].SetLineColor(color)
    h5R[ii].SetMarkerColor(color)
    h5R[ii].SetMarkerStyle(22+ii*2)
    h5R[ii].Draw("same,e")
#$ln=TLine()
#$ln.SetLineStyle(kDashed)
#$ln.SetLineColor(kYellow+2)
#$ln.DrawLine(0.,20.,15.,20)
can.SaveAs("Psi2S_over_Jpsi_Inclusive.pdf")

