import sys,os
from ROOT import *
from array import array
import root_numpy
import numpy as np
import pandas
from hep_ml import reweight
import matplotlib.pyplot as plt
from matplotlib import rcParams, style

index=0
if len(sys.argv)>1:
    index = int(sys.argv[-1])


hh=[TH1D("rr"+str(ii),"",899,1.5,900.5) for ii in range(3)]

loc="/eos/lhcb/user/z/zhangy/pA2016/psitwos_pp_2012/raw/"
fr1=TFile(loc+"data_split/FullData_sWeighted.root")
tr1=fr1.Get("DecayTree")
fr2=TFile(loc+"Psi2SMCBTagRecLight.root")
tr2=fr2.Get("DecayTree")

for evt in tr1:
    hh[0].Fill(evt.nSPDHits,evt.sig_sw)
for evt in tr2:
    #mum_isMuon&&mup_isMuon&&mum_PIDmu>3&&mup_PIDmu>3&&psi_L0DiMuonDecision_TOS&&psi_Hlt1DiMuonHighMassDecision_TOS&&psi_Hlt2DiMuonPsi2SDecision_TOS
    if evt.mum_PIDmu<3:continue
    if evt.mup_PIDmu<3:continue
    if not evt.psi_L0DiMuonDecision_TOS:continue
    if not evt.psi_Hlt1DiMuonHighMassDecision_TOS:continue
    if not evt.psi_Hlt2DiMuonPsi2SDecision_TOS:continue
    if not evt.mum_isMuon:continue
    if not evt.mup_isMuon:continue
    hh[1].Fill(evt.nSPDHits,1.)

hh[2].Divide(hh[0],hh[1])
can=TCanvas("can","",600,500)
hh[2].Draw()
can.SaveAs("SPDRatio.pdf")
fs=TFile("SPDRatio.root","recreate")
hh[2].Write()
hh[2].Fit("expo")

sys.exit()

variables_rec = [
"psi_PT",
"psi_Y",
"tz",
"tze",
"mum_eta",
"mup_eta",
"mum_P",
"mup_P",
"mum_PT",
"mup_PT",
"psi_L0MuonDecision_TOS ",
"psi_L0DiMuonDecision_TOS ",
"psi_Hlt1DiMuonHighMassDecision_TOS ",
"psi_Hlt2DiMuonPsi2SDecision_TOS ",
"psi_Hlt2DiMuonDY1Decision_TOS ",
"psi_fromB",
"nSPDHits",
"mum_isMuon",
"mum_PIDmu",
"mup_isMuon",
"mup_PIDmu",
]

variables_gen = [
"psi_PT",
"psi_Y",
"psi_fromB",
"nSPDHits"
]

columns=["nSPDHits"]

target   = root_numpy.root2array('data_split/FullData_sWeighted.root',treename='DecayTree'
        ,branches=["sig_sw","nSPDHits"],start=0,stop=6457237-100,step=10)
original = root_numpy.root2array('Psi2SMCBTagRecLight.root',treename='DecayTree'
        ,branches=["nSPDHits"],selection="mum_isMuon&&mup_isMuon&&mum_PIDmu>3&&mup_PIDmu>3&&psi_L0DiMuonDecision_TOS&&psi_Hlt1DiMuonHighMassDecision_TOS&&psi_Hlt2DiMuonPsi2SDecision_TOS")

target_wt = target['sig_sw']
original_wt = np.ones(len(original))

original  = pandas.DataFrame(original)
target    = pandas.DataFrame(target)
target    = target[["nSPDHits"]]

reweighter = reweight.GBReweighter(n_estimators=100, learning_rate=0.2, max_depth=5, min_samples_leaf=500, gb_args={'subsample': 0.6})
reweighter.fit(original, target, original_weight=original_wt, target_weight=target_wt)
print "Weight function obtained"


original_all_rec = root_numpy.root2array('Psi2SMCBTagRecLight.root',treename='DecayTree'   ,branches=variables_rec)
original_all_gen = root_numpy.root2array('Psi2SMCBTagGenLight.root',treename='MCDecayTree' ,branches=variables_gen)

original_all_rec  = pandas.DataFrame(original_all_rec)
original_all_gen  = pandas.DataFrame(original_all_gen)
original_all_rec_reduce = original_all_rec(columns)
original_all_gen_reduce = original_all_gen(columns)

orignal_all_rec_wt = np.ones(len(original_all_rec))
orignal_all_gen_wt = np.ones(len(original_all_gen))

gb_weights_rec = reweighter.predict_weights(original_all_rec_reduce, orignal_all_rec_wt)
gb_weights_gen = reweighter.predict_weights(original_all_gen_reduce, orignal_all_gen_wt)
print "Weights generated"

# Add weights to tuple
original_all_rec['gb_weights'] = gb_weights_rec
original_array = original_all_rec.to_records(False)
root_numpy.array2root(original_array, "Psi2SMCBTagRecLight_Weighted.root", mode = "recreate", treename = "DecayTree")

original_all_gen['gb_weights'] = gb_weights_gen
original_array = original_all_gen.to_records(False)
root_numpy.array2root(original_array, "Psi2SMCBTagGenLight_Weighted.root", mode = "recreate", treename = "MCDecayTree")
