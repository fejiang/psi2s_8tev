from ROOT import *
from math import atan,exp,sqrt
import sys
sys.path +=["/afs/cern.ch/user/z/zhangy/workdir/psitwos_pp_2012/ScriptsJiangFeng/code/yanxi/Analysis"]
from binning import ptbins, ybins


tag="Psi2S"
if len(sys.argv)>1:
    tag=sys.argv[-1]

feD = TFile("PerfHists_Mu_Strip21_MagDown_Fine_P_ETA_nSPDHits.root")
heD = feD.Get("Mu_DLLmu>3.&&IsMuon==1._All")
feU = TFile("PerfHists_Mu_Strip21_MagUp_Fine_P_ETA_nSPDHits.root")
heU = feU.Get("Mu_DLLmu>3.&&IsMuon==1._All")



def GetEffPID(chRec, ptmin, ptmax, ymin, ymax,tag,wtFB=1.):
    print "Tracking Correction + fromB"
    nafter=0.
    nbefore=0.
    for evt in chRec:
        wtB = 1.
        if tag=="Jpsi" and evt.psi_fromB: wtB=wtFB #weight 21 is average, 100 as an extreme
        nspd = evt.nSPDHits
        wt = exp(-2.2144+nspd*0.007712)*wtB
        nbefore  += wt # the weight matters for L0Trigger efficiency
        vpD = heD.GetBinContent(heD.FindBin(evt.mup_P,evt.mup_eta,nspd))
        vpU = heU.GetBinContent(heD.FindBin(evt.mup_P,evt.mup_eta,nspd))
        vmD = heD.GetBinContent(heD.FindBin(evt.mum_P,evt.mum_eta,nspd))
        vmU = heU.GetBinContent(heD.FindBin(evt.mum_P,evt.mum_eta,nspd))
        #error will be evaluated later at the same time for Jpsi and psi2S
        epD = heD.GetBinError(heD.FindBin(evt.mup_P,evt.mup_eta,nspd))+0.0000001 # to avoid 0, but small enough
        epU = heU.GetBinError(heD.FindBin(evt.mup_P,evt.mup_eta,nspd))+0.0000001
        emD = heD.GetBinError(heD.FindBin(evt.mum_P,evt.mum_eta,nspd))+0.0000001
        emU = heU.GetBinError(heD.FindBin(evt.mum_P,evt.mum_eta,nspd))+0.0000001
        #if ptmin<0.5 and ymin<2.3:
        #    print vm1,vm2,em1,em2

        #take the weighted average
        vp = (vpU/epU/epU+vpD/epD/epD)/(1./epU/epU+1./epD/epD)
        vm = (vmU/emU/emU+vmD/emD/emD)/(1./emU/emU+1./emD/emD)

        nafter += wt*vp*vm

    eff = nafter/nbefore
    return [eff,0.]




fs1 = open("pid"+tag+".txt","a+")
#for y,_ in enumerate(ybins[:len(ybins)-1]): # y in rest frame, take minius sign here for convenience
#    for pt,_ in enumerate(ptbins[:len(ptbins)-1]):
for y in range(len(ybins)-1):
    for pt in range(len(ptbins)-1):
        #if y!=0 or pt !=0: continue
        if y<2:continue
        name = "_%i_%i_%i"%(y,ptbins[pt],ptbins[pt+1])
        chRec = TChain("DecayTree")
        chRec.Add("/eos/lhcb/user/z/zhangy/pA2016/psitwos_pp_2012/raw/data_split/"+tag+"MCBTagRecLight"+name+".root");
        print "total number of entries: ", chRec.GetEntries()

        print ptbins[pt], ptbins[pt+1],ybins[y],ybins[y+1]
        value = GetEffPID(chRec,ptbins[pt]*1000., ptbins[pt+1]*1000.,ybins[y],ybins[y+1],tag,20.)
        fs1.write("{0} {1} {2} {3}".format(ptbins[pt],ptbins[pt+1],ybins[y],ybins[y+1])+" {0:.4f} {1:.4f}\n".format(value[0],value[1]))
        print y, pt, value

fs1.close()
