#SetupUrania v4r0
##114000 for another TCK list
##120000 for another TCK list

python MakePerfHistsRunRange.py --cuts="runNumber>=120000" --binSchemeFile="ex_customBinning.py" --schemeName="Fine" "21" "MagDown" "Mu" "[DLLmu>3.&&IsMuon==1.]" "P" "ETA" "nSPDHits"&
python MakePerfHistsRunRange.py --cuts="runNumber>=120000" --binSchemeFile="ex_customBinning.py" --schemeName="Fine" "21" "MagUp" "Mu" "[DLLmu>3.&&IsMuon==1.]" "P" "ETA" "nSPDHits"&
#python MakePerfHistsRunRange.py  --binSchemeFile="ex_customBinning.py" --schemeName="Fine" "20" "MagDown" "Mu" "[DLLmu>3.&&IsMuon==1.]" "P" "ETA" "nSPDHits"&
