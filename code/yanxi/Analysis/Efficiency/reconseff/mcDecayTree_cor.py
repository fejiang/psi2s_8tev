import ROOT
from ROOT import TFile, TTree, TChain
import numpy
import root_numpy
import pandas
from hep_ml import reweight
import pylab
from pylab import subplot, hist, title
import sys

columns = ['nSPDHits']
myweightcol = ['sig_sw']

print "-------------------------------------"
print "#          Loading data             #"
print "-------------------------------------"
print "                                     "

original = root_numpy.root2array('../../selmc/seltrigger.root', branches=columns, treename="DecayTree")
originalwe = root_numpy.root2array('./MC.root', branches=columns, treename="mct/MCDecayTree")
target = root_numpy.root2array('../../massfitdata/splots/*.root', branches=columns, treename="DecayTree")
myweight = root_numpy.root2array('../../massfitdata/splots/*.root', branches=myweightcol, treename="DecayTree")
# mcweight = root_numpy.root2array('../Sep18mc.root', branches=myweightcol, treename="DecayTree")

original = pandas.DataFrame(original)
target = pandas.DataFrame(target)
originalwe = pandas.DataFrame(originalwe)
#myweight = pandas.DataFrame(myweight).T

# original_weights = numpy.zeros(len(mcweight), dtype=float)
# mcweight_array = numpy.array(len(mcweight), dtype=float)
# for i0 in range(len(mcweight)):
#    mcweight_array = mcweight[i0]
#    original_weights[i0] = mcweight_array[0]

oriweight = numpy.ones(len(original), dtype=float)
sweight = numpy.zeros(len(myweight), dtype=float)
sweight_array = numpy.array(len(myweight), dtype=float)
for i0 in range(len(myweight)):
   sweight_array = myweight[i0]
   sweight[i0] = sweight_array[0]

print "-------------------------------------"
print "#        Calculate weights          #"
print "-------------------------------------"
print "                                     "

#count binned weight
#bins_reweighter = reweight.BinsReweighter(n_bins=25, n_neighs=1.)
#bins_reweighter.fit(original, target, original_weights, sweight)
#bins_weights = bins_reweighter.predict_weights(original)
#bins_weightsf0 = bins_reweighter.predict_weights(originalf0)

#print "-------------------------------------"
#print "#        Binned weights Get!        #"
#print "-------------------------------------"
#print "                                     "

#count unbinned weight
#reweighter = reweight.GBReweighter(n_estimators=60, learning_rate=0.2, max_depth=5, min_samples_leaf=250,
#                                   gb_args={'subsample': 1})
reweighter = reweight.GBReweighter(n_estimators=50, learning_rate=0.2, max_depth=3, min_samples_leaf=1000,
                                   gb_args={'subsample': 1.})
reweighter.fit(original, target, oriweight, sweight)
gb_weights = reweighter.predict_weights(originalwe)

print "-------------------------------------"
print "#        Unbinned weights Get!      #"
print "-------------------------------------"
print "                                     "


#fill tree
chori = TChain('mct/MCDecayTree','')
chori.Add('./MC.root')

GBweight = numpy.zeros(1, dtype=float)

print "-------------------------------------"
print "#        Saving Lbmc trees          #"
print "-------------------------------------"
print "                                     "

f1 = TFile("MCDecayTreeGB.root",'recreate')
tree_new = chori.CloneTree(0)
tree_new.Branch('GBweights', GBweight, 'GBweights/D')
for i in range(len(originalwe)):
   chori.GetEntry(i)
   GBweight[0] = gb_weights[i]
   tree_new.Fill()
tree_new.Write()
f1.Close()

print "-------------------------------------"
print "#        Have a nice day!		  #"
print "-------------------------------------"
print "                                     "

'''
from hep_ml.metrics_utils import ks_2samp_weighted
hist_settings = {'bins': 100, 'normed': True, 'alpha': 0.7}

def draw_distributions(new_original_weights):
    for id, column in enumerate(columns, 1):
        xlim = numpy.percentile(numpy.hstack([target[column]]), [0.01, 99.99])
        subplot(2, 3, id)
        hist(original[column], weights=new_original_weights, range=xlim, **hist_settings)
        hist(target[column], range=xlim, **hist_settings)
        title(column)
        print 'KS over ', column, ' = ', ks_2samp_weighted(original[column], target[column],
                                         weights1=new_original_weights, weights2=sweight)

draw_distributions(gb_weights)

pylab.show()
'''
