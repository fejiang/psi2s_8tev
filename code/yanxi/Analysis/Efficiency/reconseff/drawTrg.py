from ROOT import *
import sys
sys.path +=["/afs/cern.ch/user/z/zhangy/workdir/psitwos_pp_2012/ScriptsJiangFeng/code/yanxi/Analysis"]
from formats import colors,markers


ybins  = [2.0+ii*.5 for ii in range(6)]
ptbins = range(9) + [10, 14, 20]
ptbins = [ii*1000. for ii in ptbins]

gROOT.ProcessLine(".x ~/lhcbStyle.C")

fr=open("triggerJpsi.txt")
hh=[TH1D("htrg"+str(ii),"",10,0,1) for ii in range(5)]

from array import array
tmp=array('d',ptbins)
for h in hh:
    h.SetBins(11,tmp)

index = -1
for ln in fr.readlines():
    index +=1
    dt =ln.strip().split()
    ybin = index/11
    ptbin = index-11*(index/11)
    vv = float(dt[4])
    ee = float(dt[5])
    hh[ybin].SetBinContent(ptbin+1,vv)
    hh[ybin].SetBinError(ptbin+1,ee)

can=TCanvas("can","",600,500)
leg=TLegend(0.4,0.3,0.7,0.6)
leg.SetTextSize(0.05)
leg.SetTextFont(132)
leg.AddEntry(hh[0],"2.0 < #it{y} < 2.5","lpe")
leg.AddEntry(hh[1],"2.5 < #it{y} < 3.0","lpe")
leg.AddEntry(hh[2],"3.0 < #it{y} < 3.5","lpe")
leg.AddEntry(hh[3],"3.5 < #it{y} < 3.5","lpe")
leg.AddEntry(hh[4],"4.0 < #it{y} < 4.5","lpe")
for ii in range(5):
    hh[ii].SetLineColor(colors[ii])
    hh[ii].SetMarkerColor(colors[ii])
    hh[ii].SetMarkerStyle(markers[ii])
hh[0].Draw("same,e")
hh[0].SetMinimum(0.)
hh[0].SetMaximum(1.)
for ii in range(1,5):
    hh[ii].Draw("same,e")
leg.Draw("same")
can.SaveAs("TrgEffJpsi.pdf")
fw=TFile("TrgEffJpsi.root","recreate")
for h in hh:
    h.Write()
