from ROOT import *
from math import atan,exp,sqrt
import sys
sys.path +=["/afs/cern.ch/user/z/zhangy/workdir/psitwos_pp_2012/ScriptsJiangFeng/code/yanxi/Analysis"]
from binning import ptbins, ybins


tag="Psi2S"
if len(sys.argv)>1:
    tag=sys.argv[-1]

fco=TFile("ratio2012S20.root")
hco=fco.Get("Ratio")


def GetEffRec(chRec, chGen, ptmin, ptmax, ymin, ymax,tag="Psi2S",wtFB=1.):
    print "Tracking Correction + fromB"
    nafter=0.
    scaleA=0.
    for evt in chRec:
        wtB = 1. #the weight concerns the multiplicity dependent efficiency
        if tag=="Jpsi" and evt.psi_fromB: wtB=wtFB #weight 21 is average, 100 as an extreme
        if tag=="Psi2S" and abs(evt.psi_M-3686)>200.: continue
        if tag=="Psi2S" and ptmin>1500. and abs(evt.psi_M-3686)>100.: continue
        if tag=="Jpsi"  and abs(evt.psi_M-3096.9)>100.: continue
        if evt.psi_Y<ymin or evt.psi_Y>ymax:continue
        if evt.psi_PT<ptmin or evt.psi_PT>ptmax:continue
        p1 = min(evt.mum_P/1000.,199.)
        p2 = min(evt.mup_P/1000.,199.)
        corr1 = hco.GetBinContent(hco.FindBin(p1,evt.mum_eta))
        corr2 = hco.GetBinContent(hco.FindBin(p2,evt.mup_eta))
        wt = exp(-2.2144+evt.nSPDHits*0.007712)*corr1*corr2*wtB
        nafter += wt
        scaleA += pow(wt,2.)
    nbefore=0.
    scaleB=0.
    for evt in chGen:
        wtB = 1.
        if tag=="Jpsi" and evt.psi_fromB: wtB=wtFB
        if evt.psi_Y<ymin or evt.psi_Y>ymax:continue
        if evt.psi_PT<ptmin or evt.psi_PT>ptmax:continue
        if atan(evt.mup_TRUEPT/evt.mup_TRUEP_Z)<0.01:continue
        if atan(evt.mup_TRUEPT/evt.mup_TRUEP_Z)>0.4:continue
        if atan(evt.mum_TRUEPT/evt.mum_TRUEP_Z)<0.01:continue
        if atan(evt.mum_TRUEPT/evt.mum_TRUEP_Z)>0.4:continue
        wt = exp(-2.2144+evt.nSPDHits*0.007712)*wtB
        nbefore += wt
        scaleB += pow(wt,2.)

    print nbefore,nafter
    scaleA = sqrt(scaleA/nafter)
    scaleB = sqrt(scaleB/nbefore)
    print scaleA, scaleB
    eff = nafter/nbefore
    err = sqrt(nafter*(1.-eff))/nbefore * scaleA
    return [eff,err]

def GetEffTrg(chRec, ptmin, ptmax, ymin, ymax,tag,wtFB=1.):
    print "Tracking Correction + fromB"
    nafter=0.
    nbefore=0.
    scaleA=0.
    for evt in chRec:
        wtB = 1.
        if tag=="Jpsi" and evt.psi_fromB: wtB=wtFB #weight 21 is average, 100 as an extreme
        if evt.psi_Y<ymin or evt.psi_Y>ymax:continue
        if evt.psi_PT<ptmin or evt.psi_PT>ptmax:continue
        if evt.mum_PIDmu<3:continue
        if evt.mup_PIDmu<3:continue
        if not evt.mum_isMuon:continue
        if not evt.mup_isMuon:continue
        wt = exp(-2.2144+evt.nSPDHits*0.007712)*wtB
        nbefore  += wt # the weight matters for L0Trigger efficiency
        if not (evt.psi_L0MuonDecision_TOS or evt.psi_L0DiMuonDecision_TOS):continue
        if not evt.psi_Hlt1DiMuonHighMassDecision_TOS:continue
        if "Psi2S" in tag and (not evt.psi_Hlt2DiMuonDY1Decision_TOS):continue
        if "Jpsi"  in tag and (not evt.psi_Hlt2DiMuonJPsiDecision_TOS):continue
        nafter += wt
        scaleA += pow(wt,2.)

    scaleA = sqrt(scaleA/nafter)
    print scaleA
    eff = nafter/nbefore
    err = sqrt(nafter*(1.-eff))/nbefore * scaleA
    return [eff,err]




fs1 = open("selection"+tag+".txt","w")
fs2 = open("trigger"+tag+".txt","w")
#for y,_ in enumerate(ybins[:len(ybins)-1]): # y in rest frame, take minius sign here for convenience
#    for pt,_ in enumerate(ptbins[:len(ptbins)-1]):
for y in range(len(ybins)-1):
    for pt in range(len(ptbins)-1):
        #if y!=4 or pt !=10: continue
        name = "_%i_%i_%i"%(y,ptbins[pt],ptbins[pt+1])
        chRec = TChain("DecayTree")
        chRec.Add("/eos/lhcb/user/z/zhangy/pA2016/psitwos_pp_2012/raw/data_split/"+tag+"MCBTagRecLight"+name+".root");
        chGen = TChain("MCDecayTree")
        chGen.Add("/eos/lhcb/user/z/zhangy/pA2016/psitwos_pp_2012/raw/data_split/"+tag+"MCBTagGenLight"+name+".root");
        print "total number of entries: ", chRec.GetEntries()
        print "total number of entries: ", chGen.GetEntries()

        print name,ptbins[pt], ptbins[pt+1],ybins[y],ybins[y+1]
        value = GetEffRec(chRec,chGen,ptbins[pt]*1000., ptbins[pt+1]*1000.,ybins[y],ybins[y+1],tag,20.)
        fs1.write("{0} {1} {2} {3}".format(ptbins[pt],ptbins[pt+1],ybins[y],ybins[y+1])+" {0:.4f} {1:.4f}\n".format(value[0],value[1]))
        print y, pt, value
        #trigger
        value = GetEffTrg(chRec,ptbins[pt]*1000., ptbins[pt+1]*1000.,ybins[y],ybins[y+1],tag,21)
        fs2.write("{0} {1} {2} {3}".format(ptbins[pt],ptbins[pt+1],ybins[y],ybins[y+1])+" {0:.4f} {1:.4f}\n".format(value[0],value[1]))
        print y, pt, value

fs1.close()
