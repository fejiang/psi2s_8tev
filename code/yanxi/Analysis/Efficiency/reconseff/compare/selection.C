#include "TString.h"
#include <ROOT/RDataFrame.hxx>
#include <fstream>
#include "TLorentzVector.h"
#include "TVector3.h"
#include "TROOT.h"
#include "TMath.h"
#include "Math/ProbFuncMathCore.h"
#include "ROOT/RDFHelpers.hxx"
#include "TChain.h"

void selection() {

    ROOT::EnableImplicitMT(16);
    /* ROOT::RDataFrame d("tuple/DecayTree", "/disk302/lhcb/jiangfeng/8TeV_psi2S/psi2s12Down/1.root"); */
    ROOT::RDataFrame d("DecayTree", "../DecayTree_GB.root");
    auto cutMass = [](double psi_M) {
        return psi_M>=3580.&&psi_M<=3790.;
    };
    auto cutGhost = [](double mum_TRACK_GhostProb, double mup_TRACK_GhostProb) {
        return (mum_TRACK_GhostProb<0.5)&&(mup_TRACK_GhostProb<0.5);
    };
    auto cutPT = [](double mum_PT, double mup_PT) {
        return (mum_PT>1000)&&(mup_PT>1000);
    };
    auto cutP = [](double mum_P, double mup_P) {
        return (mum_P>6000)&&(mup_P>6000);
    };
    auto cutTrackCHi2 = [](double mum_TRACK_CHI2NDOF, double mup_TRACK_CHI2NDOF) {
        return (mum_TRACK_CHI2NDOF<4)&&(mup_TRACK_CHI2NDOF<4);
    };
    auto cutProb = [](double psi_ENDVERTEX_CHI2, int psi_ENDVERTEX_NDOF) {
        return ROOT::Math::chisquared_cdf_c(psi_ENDVERTEX_CHI2,psi_ENDVERTEX_NDOF)>0.005;
    };
    auto definTz = [](double psi_ENDVERTEX_Z, double psi_OWNPV_Z, double psi_PZ) {
        return (psi_ENDVERTEX_Z-psi_OWNPV_Z)/psi_PZ*3686.1/2.9979*10;
    };
    auto definTzerr = [](double psi_ENDVERTEX_ZERR, double psi_OWNPV_ZERR, double psi_PZ) {
        return sqrt(psi_ENDVERTEX_ZERR*psi_ENDVERTEX_ZERR+psi_OWNPV_ZERR*psi_OWNPV_ZERR)/psi_PZ*3686.1/2.9979*10;
    };
    auto cutTz = [](double tz) {
        return fabs(tz)<10;
    };
    auto cutTzErr= [](double tzerr) {
        return tzerr<0.3;
    };
    auto cutPID = [](double mum_PIDmu, double mup_PIDmu) {
        return (mum_PIDmu>3&&mup_PIDmu>3);
    };
    auto cutL0 = [](bool psi_L0MuonDecision_TOS) {
        return psi_L0MuonDecision_TOS;
    };
    auto cutHLT1 = [](bool psi_Hlt1DiMuonHighMassDecision_TOS) {
        return psi_Hlt1DiMuonHighMassDecision_TOS;
    };
    auto cutHLT2 = [](double psi_PT, bool psi_Hlt2DiMuonPsi2SDecision_TOS, bool psi_Hlt2DiMuonPsi2SHighPTDecision_TOS) {
        if (psi_PT>=3500) {
            return psi_Hlt2DiMuonPsi2SHighPTDecision_TOS;
        } else {
            return psi_Hlt2DiMuonPsi2SDecision_TOS;
        }
    };
    auto cutETA = [](double mum_ETA, double mup_ETA) {
        return (mum_ETA>=2.&&mum_ETA<=4.9&&mup_ETA>=2.&&mup_ETA<=4.9);
    };
    auto defmum_ETA = [](double mum_P, double mum_PZ) {
        return 0.5*log((mum_P+mum_PZ)*1./(mum_P-mum_PZ));
    };
    auto defmup_ETA = [](double mup_P, double mup_PZ) {
        return 0.5*log((mup_P+mup_PZ)*1./(mup_P-mup_PZ));
    };
    auto cutTCKs = [](UInt_t OdinTCK) {
        return OdinTCK >= 0x94003d;
    };
    auto cutnPV = [](int nPVs) {
        return nPVs > 0;
    };
    auto cutnSPD = [](int nSPDHits) {
        return nSPDHits < 600;
    };
    auto definCostheta = [](double mum_PX, double mum_PY, double mum_PZ, double mum_PE, double psi_PX, double psi_PY, double psi_PZ, double psi_PE) {
        TLorentzVector mum(mum_PX,mum_PY,mum_PZ,mum_PE);
        TLorentzVector psi(psi_PX,psi_PY,psi_PZ,psi_PE);
        TVector3 boostvec = -psi.BoostVector();
        mum.Boost(boostvec);
        TVector3 unmum = mum.Vect().Unit();
        return unmum.Dot(psi.Vect().Unit());
    };
    auto definPhi = [](double mum_PX, double mum_PY, double mum_PZ, double mum_PE, double psi_PX, double psi_PY, double psi_PZ, double psi_PE) {
        TLorentzVector mum(mum_PX,mum_PY,mum_PZ,mum_PE);
        TLorentzVector psi(psi_PX,psi_PY,psi_PZ,psi_PE);
        TVector3 boostvec = -psi.BoostVector();
        mum.Boost(boostvec);
        TVector3 unmum = mum.Vect().Unit();
        TVector3 zax(0,0,1);
        TVector3 epsi=(psi.Vect().Cross(zax)).Unit();
        TVector3 emu=(unmum.Cross(psi.Vect())).Unit();
        double sinphi=(epsi.Cross(emu)).Dot(psi.Vect().Unit());
        return (sinphi>0. ? acos(epsi.Dot(emu)) : -acos(epsi.Dot(emu)));
    };
    auto cutMCTruth= [](int mum_TRUEID, int mup_TRUEID, int mum_MC_MOTHER_ID, int mup_MC_MOTHER_ID, int psi_TRUEID) {
        return (((mum_TRUEID==13&&mup_TRUEID==-13&&mum_MC_MOTHER_ID==100443&&mup_MC_MOTHER_ID==100443&&psi_TRUEID==100443))||((mum_TRUEID==-13&&mup_TRUEID==13&&mum_MC_MOTHER_ID==100443&&mup_MC_MOTHER_ID==100443&&psi_TRUEID==100443)));
    };


    auto filter= d.Filter(cutMass,{"psi_M"}, "cutMass")
        .Filter(cutL0, {"psi_L0MuonDecision_TOS"}, "cutL0")
        .Filter(cutHLT1, {"psi_Hlt1DiMuonHighMassDecision_TOS"}, "cutHLT1")
        .Filter(cutHLT2, {"psi_PT", "psi_Hlt2DiMuonPsi2SDecision_TOS", "psi_Hlt2DiMuonPsi2SHighPTDecision_TOS"}, "cutHLT2")
        .Filter(cutP, {"mum_P", "mup_P"}, "cutMuonP")
        .Filter(cutGhost,{"mum_TRACK_GhostProb", "mup_TRACK_GhostProb"}, "cutGhost")
        .Filter(cutProb,{"psi_ENDVERTEX_CHI2", "psi_ENDVERTEX_NDOF"}, "cutProb")
        .Filter(cutTrackCHi2,{"mum_TRACK_CHI2NDOF","mup_TRACK_CHI2NDOF"}, "cutTrackCHi2")
        .Filter(cutPT,{"mum_PT", "mup_PT"}, "cutPT")
        .Define("tz", definTz, {"psi_ENDVERTEX_Z","psi_OWNPV_Z","psi_PZ"})
        .Define("tzerr", definTzerr, {"psi_ENDVERTEX_ZERR", "psi_OWNPV_ZERR", "psi_PZ"})
        .Filter(cutTz,{"tz"}, "cutTz")
        .Filter(cutTzErr,{"tzerr"}, "cutTzErr")
        /* .Filter(cutPID,{"mum_PIDmu","mup_PIDmu"}, "cutPID") */
        .Filter(cutnPV, {"nPVs"}, "cutnPVs")
        .Filter(cutnSPD, {"nSPDHits"}, "cutnSPDHits")
        /* .Filter(cutTCKs, {"OdinTCK"}, "cutTCKs") */
        .Define("mum_ETA", defmum_ETA, {"mum_P","mum_PZ"})
        .Define("mup_ETA", defmup_ETA, {"mup_P","mup_PZ"})
        .Filter(cutETA, {"mum_ETA", "mup_ETA"}, "cutETA")
        .Filter(cutMCTruth, {"mum_TRUEID", "mup_TRUEID", "mum_MC_MOTHER_ID", "mup_MC_MOTHER_ID", "psi_TRUEID"}, "cutMCTruth");
    vector<string> vars={"GBweights", "nSPDHits"};
    filter.Snapshot("DecayTree","selgb.root",vars);
    /* cout<<"save file end. start get cut efficiency"<<endl; */
    /* filter.Report()->Print(); */
}
