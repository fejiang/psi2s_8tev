from ROOT import *
from math import sqrt


chA = TChain("MCDecayTree")
chA.Add("/afs/cern.ch/user/f/fejiang/eos/Gen_psi2SMC.root");
chB = TChain("MCDecayTree")
chB.Add("/afs/cern.ch/user/f/fejiang/eos/GenJpsiMC.root");

print "total number of entries: ", chA.GetEntries()
print "total number of entries: ", chB.GetEntries()

def GetEff(ch,ptmin, ptmax, ymin, ymax):
    y="0.5*log((psi_TRUEP_E+psi_TRUEP_Z)/(psi_TRUEP_E-psi_TRUEP_Z))";
    cuty = "({0}>{1})&&({0}<{2})".format(y,ymin,ymax)
    pt="psi_TRUEPT"
    cutpt = "({0}>{1})&&({0}<{2})".format(pt,ptmin,ptmax)

    cut = cuty +"&&" +cutpt
    nbefore =  ch.GetEntries(cut)*1.
    print cut, nbefore

    theta1 = "atan(mup_TRUEPT/mup_TRUEP_Z)"
    cut1 = "({0}<0.4)&&({0}>0.01)".format(theta1,)
    theta2 = "atan(mum_TRUEPT/mum_TRUEP_Z)"
    cut2 = "({0}<0.4)&&({0}>0.01)".format(theta2,)

    cutall = cut+ "&&" + cut1+"&&" +cut2

    nafter=  ch.GetEntries(cutall)*1.

    eff = nafter/nbefore
    err = sqrt(nafter*(1.-eff))/nbefore
    return [eff,err]



fs1 = open("acceptancePsi2S.txt","w")

#ptbins = open("../../ptbin").read().splitlines()
#ybins = open("../../ybin").read().splitlines()
ybins  = [2.0+ii*.5 for ii in range(6)]
ptbins = range(9) + [10, 14, 20]
ptbins = [ii*1000. for ii in ptbins]

#for y,_ in enumerate(ybins[:len(ybins)-1]): # y in rest frame, take minius sign here for convenience
#    for pt,_ in enumerate(ptbins[:len(ptbins)-1]):
for y in range(len(ybins)-1):
    for pt in range(len(ptbins)-1):
        print ptbins[pt], ptbins[pt+1],ybins[y],ybins[y+1]
        value = GetEff(chA,ptbins[pt], ptbins[pt+1],ybins[y],ybins[y+1])
        fs1.write("{0} {1} {2} {3}".format(ptbins[pt],ptbins[pt+1],ybins[y],ybins[y+1])+" {0:.4f} {1:.4f}\n".format(value[0],value[1]))
        print y, pt, value

fs1.close()

fs2 = open("acceptanceJpsi.txt","w")
for y in range(len(ybins)-1):
    for pt in range(len(ptbins)-1):
        print ptbins[pt], ptbins[pt+1],ybins[y],ybins[y+1]
        value = GetEff(chB,ptbins[pt], ptbins[pt+1],ybins[y],ybins[y+1])
        fs2.write("{0} {1} {2} {3}".format(ptbins[pt],ptbins[pt+1],ybins[y],ybins[y+1])+" {0:.4f} {1:.4f}\n".format(value[0],value[1]))
        print y, pt, value

fs2.close()

