from ROOT import *
gROOT.ProcessLine(".x ~/lhcbStyle.C")

from FitMass1D import FitMass1D
from FitTz1D import FitTzBKG

for ii in range(5):
    for jj in range(2,20):
        if jj >=4: continue
        name="PsiTwosDYLight_%i_%i"%(ii,jj)
        name="JpsiDiMuonJpsiLight_%i_%i"%(ii,jj)
        name="PsiTwosDiMuonPsi2SLight_%i_%i"%(ii,jj)
        fi=TFile("../raw/data_split/"+name+".root")
        tr=fi.Get("DecayTree")
        
        #FitMass1D(tr,name, 3000, 3097+110.)
        #FitMass1D(tr,name, 3000, 3200)
        FitMass1D(tr,name, 3586, 3786) 
        #FitTzBKG(tr,marker=name)




