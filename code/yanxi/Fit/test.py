from ROOT import *
range1=[3450.,3900.]
range2=[3586.,3786.]

massA   =RooRealVar("Psi_M","M(#mu^{+}#mu^{-}) [MeV/c^{2}]",(range1[0]+range1[1])/2.,range1[0],range1[1])
massB   =RooRealVar("Psi_M","M(#mu^{+}#mu^{-}) [MeV/c^{2}]",(range2[0]+range2[1])/2.,range2[0],range2[1])

print massA.getVal()
print massB.getVal()
