from ROOT import *
def FitTzBKG(tr, marker):
    ybin,ptbin= marker.split(".")[0].split("_")[1:]
    ybin, ptbin = int(ybin), int(ptbin)
    ptlow = range(21)[ptbin]
    pthigh = range(21)[ptbin+1]
    ylow  = [2.0, 2.5, 3.0, 3.5, 4.0, 4.5][ybin]
    yhigh = [2.0, 2.5, 3.0, 3.5, 4.0, 4.5][ybin+1]

    mass   =RooRealVar("Psi_M","M(#mu^{+}#mu^{-}) [MeV/c^{2}]",3686.)
    tz     =RooRealVar("tz","t_{z} [ps]",-4,4)
    tzerr  =RooRealVar("tze","tzerr",0.0001,0.3)

    # bkg tz shape 
    bkg_tau1 = RooRealVar("bkg_tau1","bkg tau1",0.1,0,10)
    bkg_tau2 = RooRealVar("bkg_tau2","bkg tau2",0.3,0,10)
    bkg_tau3 = RooRealVar("bkg_tau3","bkg tau3",0.9,0,10)
    bkg_tau4 = RooRealVar("bkg_tau4","bkg tau4",2.0,0,10)
    bkg_tau5 = RooRealVar("bkg_tau5","bkg tau5",0.8,0,10)

    bkg_frac1 = RooRealVar("bkg_frac1","frac bkg 1",0.05,0,1)
    bkg_frac2 = RooRealVar("bkg_frac2","frac bkg 2",0.05,0,1)
    bkg_frac3 = RooRealVar("bkg_frac3","frac bkg 3",0.05,0,1)
    bkg_frac4 = RooRealVar("bkg_frac4","frac bkg 4",0.05,0,1)
    bkg_frac5 = RooRealVar("bkg_frac5","frac bkg 5",0.05,0,1)

    bkg_sigma1 = RooRealVar("bkg_sigma1","bkg sigma 1",0.1,0,10)
    bkg_sigma2 = RooRealVar("bkg_sigma2","bkg sigma 2",0.1,0,10)

    par_beta = RooRealVar("par_beta","bkg res beta",0.1,0,1.)
    tz_bias_bkg = RooRealVar("tz_bias_bkg", "tz bias bkg", 0, -10,10)

    bkg_res1 = RooGaussModel("bkg_res1","bkg res 1",tz,tz_bias_bkg,bkg_sigma1, tzerr)
    bkg_res2 = RooGaussModel("bkg_res2","bkg res 2",tz,tz_bias_bkg,bkg_sigma2, tzerr)
    bkg_res  = RooAddModel("bkg_res","bkg res model",RooArgList(bkg_res1,bkg_res2),RooArgList(par_beta))

    tmp_zero     = RooRealVar("tmp_zero","rer",0)
    bkg_tz1 = RooDecay("bkg_tz1","bkg tz model 1",tz,tmp_zero, bkg_res, RooDecay.DoubleSided)
    bkg_tz2 = RooDecay("bkg_tz2","bkg tz model 2",tz,bkg_tau1, bkg_res, RooDecay.SingleSided)
    bkg_tz3 = RooDecay("bkg_tz3","bkg tz model 3",tz,bkg_tau2, bkg_res, RooDecay.SingleSided)
    bkg_tz4 = RooDecay("bkg_tz4","bkg tz model 4",tz,bkg_tau3, bkg_res, RooDecay.Flipped)
    bkg_tz5 = RooDecay("bkg_tz5","bkg tz model 5",tz,bkg_tau4, bkg_res, RooDecay.DoubleSided)
    bkg_tz6 = RooDecay("bkg_tz6","bkg tz model 6",tz,bkg_tau5, bkg_res, RooDecay.DoubleSided)

    bkg_model_tz = RooAddPdf("bkg_model_tz","bkg tz model",RooArgList(bkg_tz2,bkg_tz3,bkg_tz4,bkg_tz5,bkg_tz1),RooArgList(bkg_frac1,bkg_frac2,bkg_frac3,bkg_frac4))
    #bkg_model_tz = RooAddPdf("bkg_model_tz","bkg tz model",RooArgList(bkg_tz2,bkg_tz3,bkg_tz4,bkg_tz5,bkg_tz6,bkg_tz1),RooArgList(bkg_frac1,bkg_frac2,bkg_frac3,bkg_frac4,bkg_frac5))
    # end bkg tz shape 


    Nbkg = RooRealVar("Nbkg","bkg number",3000.,0,2000000)
    tot_model = RooExtendPdf("tot_model","",bkg_model_tz, Nbkg)

    data      =RooDataSet("data","",tr,RooArgSet(mass,tz,tzerr),"abs(Psi_M-3686.1)>70&&abs(Psi_M-3686.1)<(3900-3686.1)")
    tzerrdata =RooDataSet("tzerrdata","",tr,RooArgSet(tzerr))

    parset=tot_model.getParameters(data)
    parset.Print("V")

    tot_model.fitTo(data, RooFit.Extended(kTRUE), RooFit.NumCPU(10), RooFit.ConditionalObservables(RooArgSet(tzerr)))

    fitpar=tot_model.getParameters(data)
    fitpar.writeToFile("./txt/"+marker+"tz1D.txt") 


    frame=tz.frame(RooFit.Title(" "))
    data.plotOn(frame, RooFit.Name("Data"), RooFit.Binning(50))
    tot_model.plotOn(frame,RooFit.ProjWData(tzerrdata,kTRUE)
            ,RooFit.Name("bkg")
            ,RooFit.DrawOption("F")
            ,RooFit.LineColor(kBlue)
            ,RooFit.LineWidth(0)
            ,RooFit.FillColor(kBlue)
            ,RooFit.FillStyle(3001)
            ,RooFit.MoveToBack()
            ,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected)
            )
    frame.GetYaxis().SetTitleOffset(1.05)
    c=TCanvas("c","",10,10,800,600)
    frame.Draw()
    frame.SetMinimum(1)

    leg = TLegend(0.67,0.7,0.95,0.9)
    leg.AddEntry("mass data","Data","lpe")
    leg.AddEntry("bkg","bkg","lf")
    leg.SetFillStyle(0)
    logoleg1=TLegend(0.60,0.45,0.8,0.65)
    logoleg1.AddEntry(0,"LHCb Unofficial","C")
    logoleg1.AddEntry(0,"pp @ #sqrt{s} = 5TeV","C")
    logoleg1.AddEntry(0,"%.1f < #it{y} < %.1f"%(ylow,yhigh),"C")
    logoleg1.AddEntry(0,"%.0f < #it{p}_{T} < %.0f [MeV/c]"%( ptlow,pthigh),"C")
    logoleg1.SetTextSize(0.04)
    logoleg1.SetFillStyle(0)
    leg = TLegend(0.67,0.7,0.95,0.9)
    leg.DrawClone()

    c.SetLogy()
    c.SaveAs("./pdf/"+marker+"tz1D.pdf")


if __name__ == "__main__":
    gROOT.ProcessLine(".x ~/lhcbStyle.C")
    
    name="PsiTwosDYLight_2_2"
    fi=TFile("../raw/data_split/"+name+".root")
    tr=fi.Get("DecayTree")
    
    FitTzBKG(tr,name)




