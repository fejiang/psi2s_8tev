#include "Header.log"
void fittz1(TTree* tr, TTree *tr_tail, TString marker, double ptlow, double pthigh,double ylow, double yhigh){
    using namespace RooFit;
    //observables
    RooRealVar tz("tz","#it{t}_{z} [ps]",-2,10);
    RooRealVar tzerr("tze","tzerr",0.0001,0.3);
    RooRealVar sig_sw("sig_sw","sw",-0.);



    RooDataSet* data=new RooDataSet("data","",tr,RooArgSet(tz,tzerr,sig_sw),"2>1","sig_sw");
    RooDataSet* tzerrdata=new RooDataSet("tzerrdata","",tr,RooArgSet(tzerr));

    RooDataSet* data_tail=new RooDataSet("data_tail","",tr_tail,RooArgSet(tz));
    TH1D *hist_tail = (TH1D*) data_tail->createHistogram("hist_tail_tz",tz,Binning(100,tz.getMin(),tz.getMax()));
    RooDataHist data_hist_tz("data_hist_tz","",tz,hist_tail);
    cout<<tz.getMin()<<"\t"<<tz.getMax()<<endl;

    // fit parameters
    RooRealVar tz_bias_sig("tz_bias_sig","signal tz res mu",0.0,-5,5);
    RooRealVar tz_sigma1_sig("tz_sigma1_sig","signal tz res sigma1",0.5,0,10);
    RooRealVar tz_sigma2_sig("tz_sigma2_sig","signal tz res sigma2",1.5,0,10);
    RooRealVar tz_sigma3_sig("tz_sigma3_sig","signal tz res sigma3",3.31,0,10);
    RooRealVar sig_par_beta("sig_par_beta","sig res beta",0.7,0,1.);
    RooRealVar sig_par_gamma("sig_par_beta","sig res beta",0.2,0,1.);
    RooGaussModel sig_tz_res1("sig_tz_res1","signal res model1",tz,tz_bias_sig,tz_sigma1_sig, tzerr);
    RooGaussModel sig_tz_res2("sig_tz_res2","signal res model2",tz,tz_bias_sig,tz_sigma2_sig, tzerr);
    RooGaussModel sig_tz_res3("sig_tz_res3","signal res model3",tz,tz_bias_sig,tz_sigma3_sig, tzerr);
    RooAddModel sig_tz_res("sig_tz_res", "sig tz res model", RooArgList(sig_tz_res1, sig_tz_res2), sig_par_beta); 
    //RooAddModel sig_tz_res("sig_tz_res", "sig tz res model", RooArgList(sig_tz_res1, sig_tz_res2, sig_tz_res3), RooArgList(sig_par_beta,sig_par_gamma)); 

    RooRealVar tmp_zero("tmp_zero","rer",0);
    RooDecay prompt_sig_tz("prompt_sig_tz","delta signal",tz,tmp_zero,sig_tz_res,RooDecay::DoubleSided);

    RooRealVar fromb_tau("fromb_tau","from b tau",1.5,0.5,10);
    RooDecay fromb_sig_tz("fromb_sig_tz","from b signal in exp",tz, fromb_tau, sig_tz_res, RooDecay::SingleSided);

    RooHistPdf tail_sig_tz("tail_sig_tz","tail_signal",tz, data_hist_tz, 2);

    RooRealVar frac_tail("frac_tail","from b tau",0.0001,0.,0.1);
    RooRealVar FB("FB","from b frac",0.2,0.,1.);
    RooFormulaVar fracB("fracB","(1.-frac_tail)*FB",RooArgSet(FB,frac_tail));

    RooAddPdf tot_model("tot_model","",RooArgList(fromb_sig_tz,tail_sig_tz,prompt_sig_tz),RooArgList(fracB,frac_tail));
    //RooAddPdf tot_model("tot_model","",RooArgList(fromb_sig_tz,prompt_sig_tz),RooArgList(fracB));

    //fix taub for low-pt DY bins
    RooArgSet par_taub(fromb_tau);
    int fitybin = int(ylow/0.5-4+0.1);
    int fitptbin1 = int(ptlow+0.1);
    int fitptbin2 = int(pthigh+0.1);
    frac_tail.setVal(0.000001); frac_tail.setConstant();
    if((int(ptlow+0.1)<2) ||
            (fitybin==0 && fitptbin1==14)||
            (fitybin==4 && fitptbin1==10)||
            (fitybin==4 && fitptbin1==4)||
            (fitybin==4 && fitptbin1==8)||
            (fitybin==4 && fitptbin1==7)||
            (fitybin==3 && fitptbin1==4)||
            (fitptbin1==14)||
            //(fitybin==1 && fitptbin1==9)||
           0 
            ){
        par_taub.readFromFile(TString::Format("txt/mcPsi2SMCBTagRecLight_%i_%i_%i_sFit.txt",int(ylow/0.5-4+0.1), int(ptlow+0.1),int(pthigh+0.1)));
        fromb_tau.setConstant();
        sig_par_beta.setVal(0.999999);sig_par_beta.setConstant();
        tz_sigma2_sig.setVal(1.);tz_sigma2_sig.setConstant();
        //tz_sigma1_sig.setConstant();
        //tz_bias_sig.setConstant();
        frac_tail.setVal(0.000001); frac_tail.setConstant();
    }


    tot_model.fitTo(*data,Save(), NumCPU(10), ConditionalObservables(tzerr),SumW2Error(kTRUE));

    RooArgSet *fitpar=tot_model.getParameters(*data);
    fitpar->writeToFile("./txt/"+marker+"_sFit.txt"); 



    TCanvas* c=new TCanvas("c","",10,10,800,600);
    TLegend* logoleg1=new TLegend(0.25,0.70,0.50,0.85);
    logoleg1->SetTextFont(132);
    //logoleg1->AddEntry((TObject*)0,"LHCb Unofficial","C");
    logoleg1->AddEntry((TObject*)0,"pp @ #sqrt{s} = 8TeV","C");
    logoleg1->AddEntry((TObject*)0,Form("%.1f < #it{y} < %.1f", ylow,yhigh),"C");
    logoleg1->AddEntry((TObject*)0,Form("%.0f < #it{p}_{T} < %.0f [GeV/c]", ptlow,pthigh),"C");
    logoleg1->SetTextSize(0.04);
    logoleg1->SetFillStyle(0);


    RooPlot* frame=tz.frame(Title(" "));
    data->plotOn(frame, Name("tzdata"), Binning(100),DataError(RooAbsData::SumW2));
    tot_model.plotOn(frame,ProjWData(*tzerrdata,kTRUE), Name("curve"),LineColor(2) );
    tot_model.plotOn(frame,ProjWData(*tzerrdata,kTRUE), Name("promptsignal"), Components("prompt_sig_tz"), LineColor(0),LineWidth(0),DrawOption("F"),FillColor(kYellow+1),FillStyle(3001));
    tot_model.plotOn(frame,ProjWData(*tzerrdata,kTRUE), Name("fromb"), Components("fromb_sig_tz")        , LineColor(0),LineWidth(0),DrawOption("F"),FillColor(kBlue+2)  ,FillStyle(3444));
    tot_model.plotOn(frame,ProjWData(*tzerrdata,kTRUE), Name("tail"), Components("tail_sig_tz")          , LineColor(0),LineWidth(0),DrawOption("F"),FillColor(kCyan)    ,FillStyle(3001));
    tot_model.plotOn(frame,ProjWData(*tzerrdata,kTRUE), Name("curve"),LineColor(2) );
    frame->GetYaxis()->SetTitleOffset(1.05);
    frame->Draw();
    frame->SetMinimum(1);

    TLegend leg(0.60,0.7,0.90,0.9);
    leg.SetTextFont(132);
    leg.AddEntry("tzdata","Data","lpe");
    leg.AddEntry("curve","Fit","l");
    leg.AddEntry("promptsignal","Prompt #psi(2S)","f");
    leg.AddEntry("fromb","#psi(2S) from b","f");
    leg.AddEntry("tail","Wrong PV","f");
    leg.SetFillStyle(0);
    leg.DrawClone();
    logoleg1->Draw();

    c->SetLogy();
    c->SaveAs("./pdf/"+marker+"tz_sFit.pdf");

}

void TzsFit(int index = 1){
    gROOT->ProcessLine(".x ~/lhcbStyle.C");
    TString name;
    TString name_tail;
    //float ptbins[12] = {0., 1., 2., 3., 4., 5., 6., 7., 8., 10., 14., 20.};
    int ptbins[12] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 14, 20};
    for(int i=0;i<5;i++){
        for(int j=0;j<11;j++){
            if(i!=4||j!=9) continue;
            if(j<2) continue;
            if(index==1) {
                name_tail.Form("JpsiDiMuonJpsiTailLight_%i_%i_%i",i,ptbins[j],ptbins[j+1]);
                name.Form("Jpsi_%i_%i_%i_sWeighted",i,ptbins[j],ptbins[j+1]);
            }
            if(index==2){
                if (j>1) {
                    name.Form("Psi2SDiMuon_%i_%i_%i_sWeighted",i,ptbins[j],ptbins[j+1]);
                    name_tail.Form("PsiTwosDiMuonPsi2STailLight_%i_%i_%i",i,ptbins[j],ptbins[j+1]);
                }
                if (j<2) {
                    name.Form("Psi2SDY_%i_%i_%i_sWeighted",i,ptbins[j],ptbins[j+1]);
                    name_tail.Form("PsiTwosDYTailLight_%i_%i_%i",i,ptbins[j],ptbins[j+1]);
                }
            }
            cout<<name<<endl;
            TFile tpf_tail("/eos/lhcb/user/z/zhangy/pA2016/psitwos_pp_2012/raw/data_split/tail/"+name_tail+".root");
            TTree *tail_tree=(TTree*)tpf_tail.Get("DecayTree");

            TFile tpf("/eos/lhcb/user/z/zhangy/pA2016/psitwos_pp_2012/raw/data_split/"+name+".root");
            TTree *protree=(TTree*)tpf.Get("DecayTree");
            fittz1(protree,tail_tree,name,ptbins[j],ptbins[j+1], 2.0+i*0.5, 2.5+i*0.5);
            double sig_wt;
            protree->SetBranchAddress("sig_sw",&sig_wt);
            double wt=0., wt2=0.;
            for(int evt=0;evt<protree->GetEntries();evt++){
                protree->GetEntry(evt);
                wt += sig_wt;
                wt2 += sig_wt*sig_wt;
            }
            cout<<wt<<"\t"<<wt2<<endl;
        }
    }
}

