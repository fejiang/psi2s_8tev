FB =  0.16083 +/- 0.013701 L(0 - 1) 
frac_tail =  0.0000010000 C L(0 - 0.1) 
fromb_tau =  1.4476 +/- 0.11911 L(0.5 - 10) 
sig_par_beta =  0.93710 +/- 0.022165 L(0 - 1) 
tmp_zero =  0.0000 C L(-INF - +INF) 
tz_bias_sig =  0.055131 +/- 0.041719 L(-5 - 5) 
tz_sigma1_sig =  1.0407 +/- 0.032640 L(0 - 10) 
tz_sigma2_sig =  3.7818 +/- 0.25991 L(0 - 10) 
