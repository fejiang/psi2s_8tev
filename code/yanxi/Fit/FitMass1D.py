from ROOT import *
def FitMass1D(tr, marker,ranges=[3586.,3786.]):
    massA   =RooRealVar("psi_M","M(#mu^{+}#mu^{-}) [MeV/c^{2}]",ranges[0],ranges[1])

    # mass shape for signal
    center = 3686.097
    meanA   = RooRealVar("meanA","shared mean of CB",center,center-20.,center+20.)
    sigmaA1 = RooRealVar("sigmaA1","sigma of 1st CB",15,5,50)
    sigmaA2 = RooFormulaVar("sigmaA2","2.399*sigmaA1-4.5393",RooArgList(sigmaA1))
    alphaA1 = RooFormulaVar("alphaA1","0.02135*sigmaA1+1.80475",RooArgList(sigmaA1))
    alphaA2 = RooFormulaVar("alphaA2","0.02135*sigmaA2+1.80475",RooArgList(sigmaA2))
    #alphaA1 = RooFormulaVar("alphaA1","2.066 + 0.0085*sigmaA1-0.00011*sigmaA1*sigmaA1",RooArgList(sigmaA1))
    #alphaA2 = RooFormulaVar("alphaA2","2.066 + 0.0085*sigmaA2-0.00011*sigmaA2*sigmaA2",RooArgList(sigmaA2))
    CBA1 = RooCBShape("CBA1","1st CBA",massA,meanA,sigmaA1,alphaA1,RooFit.RooConst(1.))
    CBA2 = RooCBShape("CBA2","2nd CBA",massA,meanA,sigmaA2,alphaA2,RooFit.RooConst(1.))
    sig_massA = RooAddPdf("sigA","Signal P.D.F. of A",RooArgList(CBA1,CBA2),RooArgList(RooFit.RooConst(0.932)))

    # mass shape for background
    aA = RooRealVar("aA","aA",0.0,-0.01,0.01)
    bkg_massA=RooExponential ("bkgA","Background",massA,aA)

    # yields for signal and background
    NsigA = RooRealVar("NsigA","sig number A",tr.GetEntries()*0.2,0.,tr.GetEntries()*1.2)

    NbkgA = RooRealVar("NbkgA","bkg number A",tr.GetEntries()*0.2,0.,tr.GetEntries()*1.2)

    # pdf for A and B
    shapesA = RooArgList(bkg_massA, sig_massA)
    yieldsA = RooArgList(NbkgA, NsigA)
    tot_modelA = RooAddPdf("tot_modelA","",shapesA,yieldsA)

    # data for A and B
    dataA      =RooDataSet("dataA","",tr,RooArgSet(massA))

    # create negative likelihood, adding the two and minimize
    # There can be only one valid minimizer
    nllA = RooNLLVar("nllA", "", tot_modelA, dataA, True) 
    fitSum = RooMinimizer(nllA)
    fitSum.migrad()
    fitSum.hesse()

    #import sys
    #sys.exit()



    canMA =TCanvas("canMA","",10,10,800,600)
    mfA=massA.frame(RooFit.Title(" "))
    dataA.plotOn     (mfA, RooFit.Name("mass data"),    RooFit.Binning(50))
    tot_modelA.plotOn(mfA, RooFit.Name("mass curve"),   RooFit.LineColor(2),  RooFit.MoveToBack(),    RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    tot_modelA.plotOn(mfA, RooFit.Name("prompt signal"),RooFit.Components("sigA"), RooFit.DrawOption("F"), RooFit.LineWidth(0), RooFit.FillColor(kYellow+1),RooFit.MoveToBack(),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    tot_modelA.plotOn(mfA, RooFit.Name("bkg"),          RooFit.Components("bkgA"),  RooFit.DrawOption("F"), RooFit.LineWidth(0), RooFit.FillColor(kBlue+1),RooFit.FillStyle(3004),RooFit.MoveToBack(),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    mfA.Draw()
    leg = TLegend(0.20,0.7,0.40,0.9)
    leg.AddEntry("mass data","Data","lpe")
    leg.AddEntry("mass curve","Fit","l")
    leg.AddEntry("prompt signal","Signal","lf")
    leg.AddEntry("bkg","Background","lf")
    leg.SetFillStyle(0)
    leg.SetTextFont(132)

    lt=TLatex()
    lt.SetNDC()
    lt.SetTextFont(132)
    lt.SetTextSize(0.04)
    lt.DrawLatex(0.65,0.85,"#psi(2S), pp @ #sqrt{s} = 8TeV")

    canMA.SaveAs("./pdf/"+marker+"_mass1D.pdf")

    #""" calculate sWeights for sample A
    from array import array
    splotA = RooStats.SPlot("splotA","splotA",dataA,tot_modelA, yieldsA)
    dataA.Print("v")
    splotA.Print()
    rootname = "/eos/lhcb/user/z/zhangy/pA2016/psitwos_pp_2012/raw/data_split/"
    fvarA = TFile( rootname+marker+"_sWeighted.root", "recreate")
    VartreeA = tr.CloneTree(0)

    sig_sw =  array("d",[0.])
    VartreeA.Branch("sig_sw",sig_sw, "sig_sw/D")
    index=-1
    for evt in tr:
        mm=getattr(evt,"psi_M")
        if mm<ranges[0] or mm>ranges[1]: continue
        index +=1
        #sig_sw[0] = dataA.get(index).find("NsigA_sw").getVal()
        sig_sw[0] = (dataA.get(index)).find("NsigA_sw").getVal()
        VartreeA.Fill()
    VartreeA.Write()
    fvarA.Close()

if __name__ == "__main__":
    gROOT.ProcessLine(".x ~/lhcbStyle.C")
    
    fiA=TFile("/afs/cern.ch/user/f/fejiang/eos/seldata.root")
    tr=fiA.Get("DecayTree")
    FitMass1D(tr,marker="FullData",ranges=[3586.,3786.])
