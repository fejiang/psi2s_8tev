from ROOT import *
def fittzmm(tr, marker):
    ybin, ptbin = marker.split(".")[0].split("_")[1:]
    ybin, ptbin = int(ybin), int(ptbin)
    ptlow = range(21)[ptbin]
    pthigh = range(21)[ptbin+1]
    ylow  = [2.0, 2.5, 3.0, 3.5, 4.0, 4.5][ybin]
    yhigh = [2.0, 2.5, 3.0, 3.5, 4.0, 4.5][ybin+1]

    masslow = 3450
    massup = 3900
    mass   =RooRealVar("Psi_M","M(#mu^{+}#mu^{-}) [MeV/c^{2}]",masslow,massup)
    tz     =RooRealVar("tz","t_{z} [ps]",-10,10)
    tzerr  =RooRealVar("tze","tzerr",0.0001,0.3)

    # signal tz shape
    Fb = RooRealVar("Fb","from b fraction",0.1,0,1)

    fromb_tau = RooRealVar("fromb_tau","from b tau",0.1,0.,10)
    tail_tau = RooRealVar("tail_tau","tail tau",10.,0.,40)

    tz_bias_sig   = RooRealVar("tz_bias_sig","signal tz res mu",0.01,-5,5)
    tz_sigma1_sig = RooRealVar("tz_sigma1_sig","signal tz res sigma1",1.31,0,10)
    tz_sigma2_sig = RooRealVar("tz_sigma2_sig","signal tz res sigma2",0.67,0,10)

    sig_par_beta = RooRealVar("sig_par_beta","sig res beta",0.5,0,1.)
    sig_tz_res1  = RooGaussModel("sig_tz_res1","signal res model1",tz,tz_bias_sig,tz_sigma1_sig, tzerr)
    sig_tz_res2  = RooGaussModel("sig_tz_res2","signal res model2",tz,tz_bias_sig,tz_sigma2_sig, tzerr)
    sig_tz_res   = RooAddModel("sig_tz_res", "sig tz res model", RooArgList(sig_tz_res1, sig_tz_res2), RooArgList(sig_par_beta))
    tmp_zero     = RooRealVar("tmp_zero","rer",0)

    prompt_sig_tz = RooDecay("prompt_sig_tz","delta signal",tz,tmp_zero,sig_tz_res,RooDecay.DoubleSided)
    fromb_sig_tz  = RooDecay("fromb_sig_tz","from b signal",tz, fromb_tau, sig_tz_res, RooDecay.SingleSided)
    tail_sig_tz  = RooDecay("tail_sig_tz","tail signal",tz, tail_tau, sig_tz_res, RooDecay.DoubleSided)
    # end signal tz shape 

    # bkg tz shape 
    bkg_tau1 = RooRealVar("bkg_tau1","bkg tau1",0.1,0,10)
    bkg_tau2 = RooRealVar("bkg_tau2","bkg tau2",0.1,0,10)
    bkg_tau3 = RooRealVar("bkg_tau3","bkg tau3",0.1,0,10)
    bkg_tau4 = RooRealVar("bkg_tau4","bkg tau4",0.8,0,10)
    bkg_tau5 = RooRealVar("bkg_tau5","bkg tau5",0.8,0,10)

    bkg_frac1 = RooRealVar("bkg_frac1","frac bkg 1",0.1,0,1)
    bkg_frac2 = RooRealVar("bkg_frac2","frac bkg 2",0.1,0,1)
    bkg_frac3 = RooRealVar("bkg_frac3","frac bkg 3",0.1,0,1)
    bkg_frac4 = RooRealVar("bkg_frac4","frac bkg 4",0.01,0,1)
    bkg_frac5 = RooRealVar("bkg_frac5","frac bkg 5",0.01,0,1)

    bkg_sigma1 = RooRealVar("bkg_sigma1","bkg sigma 1",0.1,0,10)
    bkg_sigma2 = RooRealVar("bkg_sigma2","bkg sigma 2",0.1,0,10)

    par_beta = RooRealVar("par_beta","bkg res beta",0.1,0,1.)
    tz_bias_bkg = RooRealVar("tz_bias_bkg", "tz bias bkg", 0, -10,10)

    bkg_res1 = RooGaussModel("bkg_res1","bkg res 1",tz,tz_bias_bkg,bkg_sigma1, tzerr)
    bkg_res2 = RooGaussModel("bkg_res2","bkg res 2",tz,tz_bias_bkg,bkg_sigma2, tzerr)
    bkg_res  = RooAddModel("bkg_res","bkg res model",RooArgList(bkg_res1,bkg_res2),RooArgList(par_beta))

    bkg_tz1 = RooDecay("bkg_tz1","bkg tz model 1",tz,tmp_zero, bkg_res, RooDecay.DoubleSided)
    bkg_tz2 = RooDecay("bkg_tz2","bkg tz model 2",tz,bkg_tau1, bkg_res, RooDecay.SingleSided)
    bkg_tz3 = RooDecay("bkg_tz3","bkg tz model 3",tz,bkg_tau2, bkg_res, RooDecay.SingleSided)
    bkg_tz4 = RooDecay("bkg_tz4","bkg tz model 4",tz,bkg_tau3, bkg_res, RooDecay.Flipped)
    bkg_tz5 = RooDecay("bkg_tz5","bkg tz model 5",tz,bkg_tau4, bkg_res, RooDecay.DoubleSided)

    bkg_model_tz = RooAddPdf("bkg_model_tz","bkg tz model",RooArgList(bkg_tz2,bkg_tz3,bkg_tz4,bkg_tz5,bkg_tz1),RooArgList(bkg_frac1,bkg_frac2,bkg_frac3,bkg_frac4))
    # end bkg tz shape 

    # mass shape 
    mean   = RooRealVar("mean","shared mean of CB",3686.097,3666,3706)
    sigma1 = RooRealVar("sigma1","sigma of 1st CB",15,0,30)
    n1 = RooRealVar("n1","n of first CB",1.)
    alpha1 = RooFormulaVar("alpha1","2.066 + 0.0085*sigma1-0.00011*sigma1*sigma1",RooArgList(sigma1))
    CB1 = RooCBShape("CB1","1st CB",mass,mean,sigma1,alpha1,n1)

    sigma2 = RooFormulaVar("sigma2","sigma1+25.7",RooArgList(sigma1))
    n2 = RooRealVar("n2","n of 2nd CB",1)
    alpha2 = RooFormulaVar("alpha2","2.066 + 0.0085*sigma2-0.00011*sigma2*sigma2",RooArgList(sigma2))
    CB2 = RooCBShape("CB2","2nd CB",mass,mean,sigma2,alpha2,n2)

    sig1frac = RooRealVar("sig1frac","fraction of CB1",0.96)
    sig_mass = RooAddPdf("sig","Signal P.D.F.",RooArgList(CB1,CB2),RooArgList(sig1frac))

    a1 = RooRealVar("a1","a1",0.,-0.01,0.01)
    bkg_mass=RooExponential ("bkg","Background",mass,a1)

    prompt_sig = RooProdPdf("prompt_sig","prompt signal",RooArgList(sig_mass, prompt_sig_tz))
    fromb_sig  = RooProdPdf("fromb_sig","from b",RooArgList(sig_mass, fromb_sig_tz))
    tail_sig  = RooProdPdf("tail_sig","tail",RooArgList(sig_mass, tail_sig_tz))
    sig_model = RooAddPdf("sig_model","signal dist",RooArgList(fromb_sig, prompt_sig),RooArgList(Fb))
    bkg_model = RooProdPdf ("bkg_model","bkg",RooArgList(bkg_mass, bkg_model_tz))
    # end mass shape 


    # Nsig = RooRealVar("Nsig","sig number",3000.,0,2000000) 
    Nprompt = RooRealVar("Nprompt","prompt signal",tr.GetEntries()*0.2,0,tr.GetEntries()*1.2)
    Nfromb = RooRealVar("Nfromb","from b signal"  ,tr.GetEntries()*0.2,0,tr.GetEntries()*1.2)
    Nbkg = RooRealVar("Nbkg","bkg number"         ,tr.GetEntries()*0.2,0,tr.GetEntries()*1.2)
    Ntail = RooRealVar("Ntail","tailnumber"       ,tr.GetEntries()*0.02,0,tr.GetEntries()*0.2)
    shapes = RooArgList(bkg_model, prompt_sig, fromb_sig,tail_sig)
    yields = RooArgList(Nbkg, Nprompt, Nfromb,Ntail)

    tot_model = RooAddPdf("tot_model","",shapes,yields)

    data      =RooDataSet("data","",tr,RooArgSet(mass,tz,tzerr))
    tzerrdata =RooDataSet("tzerrdata","",tr,RooArgSet(tzerr))

    parset=tot_model.getParameters(data)
    parset.readFromFile("./txt/"+marker+"Mass1D.txt")
    parset.readFromFile("./txt/"+marker+"tz1D.txt")
    #parset.Print("V")

    bkg_frac1.setConstant()
    bkg_frac2.setConstant()
    bkg_frac3.setConstant()
    bkg_frac4.setConstant()
    bkg_tau1.setConstant()
    bkg_tau2.setConstant()
    bkg_tau3.setConstant()
    bkg_tau4.setConstant()
    bkg_sigma1.setConstant()
    bkg_sigma2.setConstant()
    par_beta.setConstant()
    tz_bias_bkg.setConstant()

    #tot_model.fitTo(data, RooFit.Extended(kTRUE), RooFit.NumCPU(2), RooFit.ConditionalObservables(RooArgSet(tzerr)))

    fitpar=tot_model.getParameters(data)
    fitpar.writeToFile("./txt/"+marker+"2D.txt") 


    cmass=TCanvas("cmass","",10,10,800,600)

    massframe=mass.frame(RooFit.Title(" "))
    data.plotOn     (massframe, RooFit.Name("mass data"),    RooFit.Binning(50))
    tot_model.plotOn(massframe, RooFit.Name("mass curve"),   RooFit.LineColor(2),             RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    tot_model.plotOn(massframe, RooFit.Name("prompt signal"),RooFit.Components("prompt_sig"), RooFit.DrawOption("F"), RooFit.LineWidth(0),
            RooFit.FillColor(6),RooFit.MoveToBack(),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    tot_model.plotOn(massframe, RooFit.Name("from b"),       RooFit.Components("fromb_sig"),  RooFit.LineColor(1),    RooFit.LineStyle(9),
            RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    tot_model.plotOn(massframe, RooFit.Name("bkg"),          RooFit.Components("bkg_model"),  RooFit.DrawOption("F"), RooFit.LineWidth(0),
            RooFit.FillColor(kBlue),RooFit.FillStyle(3004),RooFit.MoveToBack(),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    massframe.Draw()
    leg = TLegend(0.67,0.7,0.95,0.9)
    leg.AddEntry("mass data","Data","lpe")
    leg.AddEntry("mass curve","Fit","l")
    leg.AddEntry("prompt signal","Prompt #psi(2S)","lf")
    leg.AddEntry("from b","#psi(2S) from b","l")
    leg.AddEntry("bkg","bkg","lf")
    leg.SetFillStyle(0)

    logoleg=TLegend(0.65,0.76,0.90,.9)
    logoleg.SetTextFont(132)
    #logoleg.AddEntry(0,"LHCb Unofficial","C")
    logoleg.AddEntry(0,"pp @ #sqrt{s} = 8TeV","C")
    logoleg.AddEntry(0,"%.1f < #it{y} < %.1f"%(ylow,yhigh),"C")
    logoleg.AddEntry(0,"%.0f < #it{p}_{T} < %.0f [GeV/c]"%( ptlow,pthigh),"C")
    logoleg.SetTextSize(0.04)
    logoleg.Draw()
    leg.Draw()

    cmass.SaveAs("./pdf/"+marker+"mass2DFit.pdf")

    c=TCanvas("c","",10,10,800,600)
    logoleg1=TLegend(0.60,0.45,0.8,0.65)
    logoleg.SetTextFont(132)
    #logoleg1.AddEntry(0,"LHCb Unofficial","C")
    logoleg1.AddEntry(0,"pp @ #sqrt{s} = 5TeV","C")
    logoleg1.AddEntry(0,"%.1f < #it{y} < %.1f"%(ylow,yhigh),"C")
    logoleg1.AddEntry(0,"%.0f < #it{p}_{T} < %.0f [GeV/c]"%( ptlow,pthigh),"C")
    logoleg1.SetTextSize(0.04)
    logoleg1.SetFillStyle(0)


    frame=tz.frame(RooFit.Title(" "))
    data.plotOn(frame, RooFit.Name("tzdata"), RooFit.Binning(50))
    tot_model.plotOn(frame,RooFit.ProjWData(tzerrdata,kTRUE), RooFit.Name("curve")       , RooFit.LineColor(2),
            RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    tot_model.plotOn(frame,RooFit.ProjWData(tzerrdata,kTRUE), RooFit.Name("promptsignal"), RooFit.Components("prompt_sig"),
            RooFit.DrawOption("F"),RooFit.LineWidth(0),RooFit.FillColor(6),RooFit.MoveToBack(),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    tot_model.plotOn(frame,RooFit.ProjWData(tzerrdata,kTRUE), RooFit.Name("fromb")       , RooFit.Components("fromb_sig"),
            RooFit.LineColor(1),RooFit.LineStyle(9),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    tot_model.plotOn(frame,RooFit.ProjWData(tzerrdata,kTRUE), RooFit.Name("bkg")         ,
            RooFit.Components("bkg_model"),RooFit.DrawOption("F"),RooFit.LineColor(kBlue),RooFit.LineWidth(0),RooFit.FillColor(kBlue),RooFit.FillStyle(3004),RooFit.MoveToBack(),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    tot_model.plotOn(frame,RooFit.ProjWData(tzerrdata,kTRUE), RooFit.Name("tail")         ,
            RooFit.Components("tail_sig"),RooFit.DrawOption("F"),RooFit.LineColor(kMagenta),RooFit.LineWidth(0),RooFit.FillColor(kMagenta),RooFit.FillStyle(1),RooFit.MoveToBack(),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    frame.GetYaxis().SetTitleOffset(1.05)
    frame.Draw()
    frame.SetMinimum(1)

    leg.DrawClone()
    logoleg1.Draw()

    c.SetLogy()
    c.SaveAs("./pdf/"+marker+"tz2DFit.pdf")


gROOT.ProcessLine(".x ~/lhcbStyle.C")

name="PsiTwosDYLight_2_2"
fi=TFile("../raw/data_split/"+name+".root")
tr=fi.Get("DecayTree")

from FitMass1D import FitMass1D
from FitTz1D import FitTzBKG
FitMass1D(tr,marker=name)
FitTzBKG(tr,marker=name)

fittzmm(tr,marker=name)



