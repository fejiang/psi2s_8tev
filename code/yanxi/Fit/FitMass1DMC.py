from ROOT import *
import sys

def FitMassJpsi_Psi2S(tr, bins, marker,ratio=False,range1=[3450.,3900.],range2=[3586.,3786.]):
    trA = tr[0]
    trB = tr[1]
    ptlow = bins[1]
    pthigh = bins[2]
    ylow  = [2.0, 2.5, 3.0, 3.5, 4.0, 4.5][bins[0]]
    yhigh = [2.0, 2.5, 3.0, 3.5, 4.0, 4.5][bins[0]+1]
    fitbin_tag = "%i_%i_%i"%tuple(bins)

    if ratio: fitbin_tag+="_ratio"

    massA   =RooRealVar("psi_M","M(#mu^{+}#mu^{-}) [MeV/c^{2}]",range1[0],range1[1])
    #massB   =RooRealVar("Jpsi_M","M(#mu^{+}#mu^{-}) [MeV/c^{2}]",range2[0],range2[1])

    # mass shape for signal
    center = 3686.097
    meanA   = RooRealVar("meanA","shared mean of CB",center,center-20.,center+20.)
    sigmaA1 = RooRealVar("sigmaA1","sigma of 1st CB",10,1,50)
    sigmaA2 = RooFormulaVar("sigmaA2","2.399*sigmaA1-4.5393",RooArgList(sigmaA1))
    alphaA1 = RooFormulaVar("alphaA1","0.02135*sigmaA1+1.80475",RooArgList(sigmaA1))
    alphaA2 = RooFormulaVar("alphaA2","0.02135*sigmaA2+1.80475",RooArgList(sigmaA2))
    #alphaA1 = RooFormulaVar("alphaA1","2.066 + 0.0085*sigmaA1-0.00011*sigmaA1*sigmaA1",RooArgList(sigmaA1))
    #alphaA2 = RooFormulaVar("alphaA2","2.066 + 0.0085*sigmaA2-0.00011*sigmaA2*sigmaA2",RooArgList(sigmaA2))
    CBA1 = RooCBShape("CBA1","1st CBA",massA,meanA,sigmaA1,alphaA1,RooFit.RooConst(1.))
    CBA2 = RooCBShape("CBA2","2nd CBA",massA,meanA,sigmaA2,alphaA2,RooFit.RooConst(1.))
    sig_massA = RooAddPdf("sigA","Signal P.D.F. of A",RooArgList(CBA1,CBA2),RooArgList(RooFit.RooConst(0.932)))
    NsigA = RooRealVar("NsigA","sig number A",trA.GetEntries()*0.2,0.,trA.GetEntries()*1.2)
    tot_modelA = RooExtendPdf("shapesA","",sig_massA,NsigA)
    dataA      =RooDataSet("dataA","",trA,RooArgSet(massA))
    nllA = RooNLLVar("nllA", "", tot_modelA, dataA, True) 
    fitA = RooMinimizer(nllA)
    fitA.migrad()
    fitA.hesse()
    #sigma_psitwos = [sigmaA1.getVal(),sigmaA1.getError()]
    sigmaA1.setConstant(1)
    meanA_fitpsi = meanA.getVal()
    meanA_fitpsi_error = meanA.getError()

    ################### ################### ################### ###################
    ################### ################### ################### ###################
    ################### ################### ################### ###################
    massB   =RooRealVar("psi_M","M(#mu^{+}#mu^{-}) [MeV/c^{2}]",range2[0],range2[1])
    center = 3096.900
    meanB   = RooFormulaVar("meanB","meanA/(3686.097/3096.900)",RooArgList(meanA))
    sigmaR = RooRealVar("sigmaR","sigma of 1st CB",0.8,0.3,2.)
    #sigmaB1 = RooRealVar("sigmaB1","sigma of 1st CB",15,5,50)
    sigmaB1 = RooFormulaVar("sigmaB1","sigmaA1/sigmaR",RooArgList(sigmaA1,sigmaR))
    sigmaB2 = RooFormulaVar("sigmaB2","2.399*sigmaB1-4.5393",RooArgList(sigmaB1))
    alphaB1 = RooFormulaVar("alphaB1","0.02135*sigmaB1+1.80475",RooArgList(sigmaB1))
    alphaB2 = RooFormulaVar("alphaB2","0.02135*sigmaB2+1.80475",RooArgList(sigmaB2))
    CBB1 = RooCBShape("CBB1","1st CBB",massB,meanB,sigmaB1,alphaB1,RooFit.RooConst(1.))
    CBB2 = RooCBShape("CBB2","2nd CBB",massB,meanB,sigmaB2,alphaB2,RooFit.RooConst(1.))
    sig_massB = RooAddPdf("sigB","Signal P.D.F. of B",RooArgList(CBB1,CBB2),RooArgList(RooFit.RooConst(0.932)))
    NsigB = RooRealVar("NsigB","sig number B",trB.GetEntries()*0.2,0.,trB.GetEntries()*1.2)

    tot_modelB = RooExtendPdf("shapesB","",sig_massB,NsigB)
    dataB      =RooDataSet("dataB","",trB,RooArgSet(massB))
    # There can be only one valid minimizer
    nllB = RooNLLVar("nllB", "", tot_modelB, dataB, True) 
    fitB = RooMinimizer(nllB)
    fitB.migrad()
    fitB.hesse()

    ################### ################### ################### ###################
    ################### ################### ################### ###################
    ################### ################### ################### ###################
    sigma_ratio = [sigmaR.getVal(),sigmaR.getError()]
    return sigma_ratio,3686.1/3096.9,meanA_fitpsi,meanA_fitpsi_error,meanA.getVal(),meanA.getError()



    sys.exit()
    canMA =TCanvas("canMA","",10,10,800,600)
    mfA=massA.frame(RooFit.Title(" "))
    dataA.plotOn     (mfA, RooFit.Name("mass data"),    RooFit.Binning(50))
    tot_modelA.plotOn(mfA, RooFit.Name("mass curve"),   RooFit.LineColor(2),  RooFit.MoveToBack(),    RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    tot_modelA.plotOn(mfA, RooFit.Name("prompt signal"),RooFit.Components("sigA"), RooFit.DrawOption("F"), RooFit.LineWidth(0), RooFit.FillColor(kYellow+1),RooFit.MoveToBack(),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    mfA.Draw()
    leg = TLegend(0.20,0.7,0.40,0.9)
    leg.AddEntry("mass data","Data","lpe")
    leg.AddEntry("mass curve","Fit","l")
    leg.AddEntry("prompt signal","Signal","lf")
    leg.SetFillStyle(0)
    leg.SetTextFont(132)

    lt=TLatex()
    lt.SetNDC()
    lt.SetTextFont(132)
    lt.SetTextSize(0.04)
    lt.DrawLatex(0.65,0.85,"#psi(2S), pp @ #sqrt{s} = 8TeV")
    lt.DrawLatex(0.65,0.80,"%.1f < #it{y} < %.1f"%(ylow,yhigh))
    lt.DrawLatex(0.65,0.75,"%.0f < #it{p}_{T} < %.0f [GeV/c]"%( ptlow,pthigh))

    canMA.SaveAs("./pdf/MC"+marker[0]+"_"+marker[1]+"_"+fitbin_tag+"_"+marker[0]+"_mass1D.pdf")

    canMB =TCanvas("canMB","",10,10,800,600)
    mfB=massB.frame(RooFit.Title(" "))
    dataB.plotOn     (mfB, RooFit.Name("mass data"),    RooFit.Binning(50))
    tot_modelB.plotOn(mfB, RooFit.Name("mass curve"),   RooFit.LineColor(2), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected), RooFit.MoveToBack())
    tot_modelB.plotOn(mfB, RooFit.Name("prompt signal"),RooFit.Components("sigB"), RooFit.DrawOption("F"), RooFit.LineWidth(0), RooFit.FillColor(kYellow+1),RooFit.MoveToBack(),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    mfB.Draw()

    lt=TLatex()
    lt.SetNDC()
    lt.SetTextFont(132)
    lt.SetTextSize(0.04)
    lt.DrawLatex(0.65,0.85,"J/#psi, pp @ #sqrt{s} = 8TeV")
    lt.DrawLatex(0.65,0.80,"%.1f < #it{y} < %.1f"%(ylow,yhigh))
    lt.DrawLatex(0.65,0.75,"%.0f < #it{p}_{T} < %.0f [GeV/c]"%( ptlow,pthigh))

    canMB.SaveAs("./pdf/MC"+marker[0]+"_"+marker[1]+"_"+fitbin_tag+"_"+marker[1]+"_mass1D.pdf")


if __name__ == "__main__":
    gROOT.ProcessLine(".x ~/lhcbStyle.C")
    
    test = True
    ptbins = range(9) + [10, 14, 20]
    rlt=[]
    fw=open("sigma_ratio.txt","w")
    for ii in range(5):
        for jj in range(len(ptbins)-1):
            #if ii!=2 or jj!=2: continue
            ptbin_low, ptbin_high = ptbins[jj],ptbins[jj+1]
            ybin_low, ybin_hig    = ii*0.5+2.0,ii*0.5+2.5
            bins = "_%i_%i_%i"%(ii,ptbin_low,ptbin_high)
            name="Psi2SMCBTagRecLight"+bins
            fiA=TFile("/eos/lhcb/user/z/zhangy/pA2016/psitwos_pp_2012/raw/data_split/"+name+".root")
            trA=fiA.Get("DecayTree")

            name="JpsiMCBTagRecLight"+bins
            fiB=TFile("/eos/lhcb/user/z/zhangy/pA2016/psitwos_pp_2012/raw/data_split/"+name+".root")
            trB=fiB.Get("DecayTree")
            rangePsi2S= [3586.,3786.]
            rangeJpsi = [3000.,3200.]
            marker = ["Psi2S","Jpsi"]
            print trA.GetEntries(),trB.GetEntries()
            arlt=FitMassJpsi_Psi2S([trA,trB],bins=[ii,ptbin_low,ptbin_high],ratio=True,marker=[marker[0], marker[1]],range1=rangePsi2S,range2=rangeJpsi)
            rlt.append(arlt)
    for arlt in rlt:
        print arlt
        fw.write(("%5.4f\t"*6+"\n")%(arlt[0][0],arlt[0][1],arlt[2],arlt[3],arlt[4],arlt[5]))
