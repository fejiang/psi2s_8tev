
vector<double> xx;
vector<double> yy;
vector<double> aa;
vector<double> bb;
vector<double> cc;
vector<double> dd;


void SplineNCubic(vector<double> x, vector<double> y,vector<double>& ai,vector<double>& bi,vector<double>& ci,vector<double>& di){
    const unsigned int dim=x.size();
    cout<<dim<<endl;
    double a[dim],b[dim-1],d[dim-1],h[dim-1],alpha[dim-1],c[dim],l[dim],mu[dim],z[dim];
    l[0]=1;
    mu[0]=0;
    z[0]=0;
    alpha[0]=0;
    for(unsigned int i=0;i<dim;i++){
        a[i]=y[i];
    }
    for(unsigned int i=0;i<dim-1;i++){
        h[i]=x[i+1]-x[i];
        if(i!=0)
            alpha[i]=3./h[i]*(a[i+1]-a[i])-3./h[i-1]*(a[i]-a[i-1]);
    }
    for(int i=1;i<dim-1;i++){
        l[i]=2.*(x[i+1]-x[i-1])-h[i-1]*mu[i-1];
        mu[i]=h[i]*1./l[i];
        z[i]=(alpha[i]-h[i-1]*z[i-1]*1.)/l[i];
    }
    l[dim-1]=1;
    z[dim-1]=0;
    c[dim-1]=0;
    for(int i=dim-2;i>=0;i--){
        cout<<i<<endl;
        c[i]=z[i]-mu[i]*c[i+1];
        b[i]=(a[i+1]-a[i])*1./h[i]-h[i]*(c[i+1]+2.*c[i])/3.;
        d[i]=(c[i+1]-c[i])/3./h[i];
    }
    cout<<"eard"<<endl;

    for(int i=0;i<dim-1;i++){
        ai.push_back(a[i]);
        bi.push_back(b[i]);
        ci.push_back(c[i]);
        di.push_back(d[i]);
    }
}

double func(double *x,double *p){
    double tmp;
    for(unsigned int i=0;i<aa.size();i++){
        if(x[0]>=xx[i]&&x[0]<=xx[i+1]){
            tmp=aa[i]+bb[i]*(x[0]-xx[i])+cc[i]*pow((x[0]-xx[i]),2)+dd[i]*pow((x[0]-xx[i]),3);
        }
    }
    return tmp;
}


void piddec(){
    gROOT->ProcessLine(".x ~/lhcbStyle.C");
    TChain* ch1=new TChain("DecayTree");
    ch1->Add("../massfitdata/sPlotFeb*.root");
    /* ch1->Add(""); */
    Double_t sig_sw,bkg_sw, mum_PIDmu, mup_PIDmu;
    ch1->SetBranchAddress("sig_sw",&sig_sw);
    ch1->SetBranchAddress("bkg_sw",&bkg_sw);
    ch1->SetBranchAddress("mum_PIDmu",&mum_PIDmu);
    ch1->SetBranchAddress("mup_PIDmu",&mup_PIDmu);

    const int bin=6;
    double maxi=5,mini=2.0;
    double step=(maxi-mini)/bin;
    double sig[bin]={0},bkg[bin]={0};
    double total=0;
    for(int i=0;i<bin;i++){
        cout<<sig[i]<<" "<<bkg[i]<<endl;
        double low=(i)*step+mini;
        for(Long64_t entry=0;entry<ch1->GetEntries();entry++){
            ch1->GetEntry(entry);
            if(!(mum_PIDmu>low&&mup_PIDmu>low)) continue;
            sig[i]+=sig_sw;
            bkg[i]+=bkg_sw;
        }
        xx.push_back(low);
        yy.push_back(sig[i]*1./sqrt(sig[i]+bkg[i]));
        total+=sig[i];
        cout<<xx[i]<<" "<<yy[i]<<endl;
    }
    cout<<"dsadas "<<sig[0]<<endl;
    SplineNCubic(xx,yy,aa,bb,cc,dd);
    for(int i=0;i<aa.size();i++){
        cout<<aa[i]<<" "<<bb[i]<<" "<<cc[i]<<" "<<dd[i]<<endl;
    }
    TCanvas* c1=new TCanvas("c1","",800,600);
    TGraph* g1=new TGraph(xx.size(),&xx[0],&yy[0]);
    g1->Draw("AC*");
    g1->SetLineWidth(2);
    g1->SetTitle(";PIDmu;S/#sqrt{S+B}");
    g1->GetYaxis()->SetTitleOffset(1.07);
    c1->SetGrid();
    TF1* fa1=new TF1("fa1",func,xx[0],xx[xx.size()-1],0);
    for(auto i : xx){
        cout<<fa1->Eval(i)<<endl;
    }
    /* fa1->Draw("same"); */
    fa1->SetLineColor(kBlue);
    fa1->SetLineStyle(kDashed);
}
