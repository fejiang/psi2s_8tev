import uproot as ur
from tensorflow import keras
import pandas as pd
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend('agg')

tf.enable_eager_execution()

sig_tree = ur.open('signal.root')['DecayTree']
bkg_tree = ur.open('bkg.root')['DecayTree']

fef = open('varf', 'r+')
features = fef.read().split('\n')
features.pop()
print(features)

df_bkg = bkg_tree.arrays(features, outputtype=pd.DataFrame)
df_sig = sig_tree.arrays(features, outputtype=pd.DataFrame)
print('========================')
print('the number of nan: ', len(df_bkg) - len(df_bkg.dropna()), len(df_sig)-len(df_sig.dropna()))
print('========================')
df_bkg = df_bkg.dropna()
df_sig = df_sig.dropna()
df = pd.concat([df_bkg, df_sig],ignore_index=True,sort=False)
# df['label'] = df['label'].apply(lambda x: 1 if x > 0 else 0)

train = df.sample(frac=0.85)
test = df.drop(train.index)

train_label = train.pop('label')
test_label = test.pop('label')

means = train.mean()
stds = train.std(ddof=0)
means.to_csv('./mean.csv')
stds.to_csv('./std.csv')

def norm(x):
    return (x - means)/stds

train = norm(train)
test = norm(test)

model = keras.Sequential([
    tf.keras.layers.Dense(128, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(0.001),input_shape=[len(train.keys())]),
    # tf.keras.layers.Dropout(0.2),
    tf.keras.layers.BatchNormalization(),
    tf.keras.layers.Dense(128, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(0.001)),
    # tf.keras.layers.Dropout(0.2),
    tf.keras.layers.BatchNormalization(),
    tf.keras.layers.Dense(128, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(0.001)),
    # tf.keras.layers.Dropout(0.2),
    tf.keras.layers.BatchNormalization(),
    tf.keras.layers.Dense(1, activation='sigmoid')
    ])

model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy', tf.keras.metrics.Precision(),tf.keras.metrics.Recall()])

early_stop = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=5)
history = model.fit(train, train_label, batch_size=1024, epochs=5, validation_split = 0.1, callbacks=[early_stop])

def plot_history(history):
  hist = pd.DataFrame(history.history)
  hist['epoch'] = history.epoch
  dvars = ['acc', 'loss', 'precision', 'recall']
  for var in dvars:
      plt.figure()
      plt.xlabel('Epoch')
      plt.ylabel(var)
      plt.plot(hist['epoch'], hist[var], label='Train' + var)
      plt.plot(hist['epoch'], hist['val_'+var], label = 'Val '+var)
      # plt.ylim([0.8,1])
      plt.legend()
      plt.savefig(var+'.png')

plot_history(history)

preds = model.predict(test)
print(preds)

print('**********')
preds = np.apply_along_axis(lambda x: 1 if x > 0.5 else 0, 1, preds)
print(preds)
# test_loss, test_acc = model.evaluate(test, test_label)
m = tf.keras.metrics.Precision()
m.update_state(test_label, preds)
print('Precision result: ', m.result().numpy())

m = tf.keras.metrics.Recall()
m.update_state(test_label, preds)
print('Recall result: ', m.result().numpy())

model.save('./my_model.h5')
