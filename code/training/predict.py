import uproot as ur
import tensorflow as tf
import pandas as pd
import numpy as np

from os import listdir
from os.path import isfile, join, dirname, basename
import multiprocessing as mp

from ROOT import TFile, TTree, TChain

# config = tf.ConfigProto()
# config.intra_op_parallelism_threads = 1
# config.inter_op_parallelism_threads = 1
# tf.Session(config=config)

tot_files = []
dirs = ['/disk302/lhcb/jiangfeng/8TeV_psi2S/psi2s12Down', '/disk302/lhcb/jiangfeng/8TeV_psi2S/psi2s12Up']
for i in dirs:
    tot_files+=[join(i, f) for f in listdir(i) if isfile(join(i, f))]

# tot_files = tot_files[:10]
# print(tot_files, len(tot_files))

new_model = tf.keras.models.load_model('./my_model.h5')
new_model.summary()

fef = open('varf', 'r+')
features = fef.read().split('\n')
fef.close()
features.pop()
features.pop()
df_mean = pd.Series.from_csv("./mean.csv")
df_std = pd.Series.from_csv("./std.csv")

for sf in tot_files:
    print('predict: ', sf)
    tree = ur.open(sf)['tuple/DecayTree']
    df = tree.arrays(features, outputtype=pd.DataFrame)
    print('============================')
    print('the number of na: ', len(df)-len(df.dropna()))
    print('============================')

    df = (df - df_mean)/df_std
    predictions = new_model.predict(df)
    labels = np.apply_along_axis(lambda x: 1 if x > 0.5 else 0, 1, predictions)
    chori = TChain('tuple/DecayTree','')
    chori.Add(sf)
    print('check consistency: ', len(labels), chori.GetEntries(), len(labels)-chori.GetEntries())
    label = np.zeros(1, dtype=int)
    f = TFile(join(dirname(sf), 'taged', basename(sf)),'recreate')
    tree_new = chori.CloneTree(0)
    tree_new.Branch('label', label, 'label/I')
    for i in range(len(labels)):
        chori.GetEntry(i)
        label[0] = labels[i]
        tree_new.Fill()
    tree_new.Write()
    f.Close()
