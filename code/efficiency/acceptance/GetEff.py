from ROOT import *
from math import sqrt


ch = TChain("MCDecayTree")
ch.Add("../../../data/Gen_psi2SMC.root");

print "total number of entries: ", ch.GetEntries()

def GetEff(ptmin, ptmax, ymin, ymax, fromb):
    if fromb:
        idcut = "&&(int(abs(psi_MC_MOTHER_ID)/100)==5 || int(abs(psi_MC_MOTHER_ID)/1000)==5)"
    else:
        idcut = "&&(psi_MC_MOTHER_ID==0)"
    y="0.5*log((psi_TRUEP_E+psi_TRUEP_Z)/(psi_TRUEP_E-psi_TRUEP_Z))";
    cuty = "({0}>{1})&&({0}<{2})".format(y,ymin,ymax)
    pt="psi_TRUEPT"
    cutpt = "({0}>{1})&&({0}<{2})".format(pt,ptmin,ptmax)

    cut = cuty +"&&" +cutpt + idcut
    nbefore =  ch.GetEntries(cut)*1.
    print cut, nbefore

    theta1 = "atan(mup_TRUEPT/mup_TRUEP_Z)"
    cut1 = "({0}<0.4)&&({0}>0.01)".format(theta1,)
    theta2 = "atan(mum_TRUEPT/mum_TRUEP_Z)"
    cut2 = "({0}<0.4)&&({0}>0.01)".format(theta2,)

    cutall = cut+ "&&" + cut1+"&&" +cut2

    nafter=  ch.GetEntries(cutall)*1.

    eff = nafter/nbefore
    err = sqrt(nafter*(1.-eff))/nbefore
    return [eff,err]



fs1 = open("acceptance_fromb.txt","w")
fs2 = open("acceptance_prompt.txt","w")

ptbins = open("../../ptbin").read().splitlines()
ybins = open("../../ybin").read().splitlines()
for y,_ in enumerate(ybins[:len(ybins)-1]): # y in rest frame, take minius sign here for convenience
    for pt,_ in enumerate(ptbins[:len(ptbins)-1]):
        print ptbins[pt], ptbins[pt+1],ybins[y],ybins[y+1]
        value = GetEff(ptbins[pt], ptbins[pt+1],ybins[y],ybins[y+1],True)
        fs1.write("{0} {1} {2} {3}".format(ptbins[pt],ptbins[pt+1],ybins[y],ybins[y+1])+" {0:.4f} {1:.4f}\n".format(value[0],value[1]))
        value = GetEff(ptbins[pt], ptbins[pt+1],ybins[y],ybins[y+1],False)
        fs2.write("{0} {1} {2} {3}".format(ptbins[pt],ptbins[pt+1],ybins[y],ybins[y+1])+" {0:.4f} {1:.4f}\n".format(value[0],value[1]))
        # print y, pt, value

fs1.close()



