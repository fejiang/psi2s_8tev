void compare(){
    gROOT->ProcessLine(".x ~/lhcbStyle.C");
    using ROOT::RDataFrame;
    ROOT::EnableImplicitMT(4);
    RDataFrame d("DecayTree", "./selgb.root");
    RDataFrame ddata("DecayTree", "../../../massfitdata/splots/*.root");
    auto nSPDMC = d.Histo1D({"nSPDMC", "MC" , 100,0, 600}, "nSPDHits"); 
    auto nSPDMCGB = d.Histo1D({"nSPDMCGB", "weighted MC" , 100,0, 600}, "nSPDHits", "GBweights"); 
    auto nSPDData = ddata.Histo1D({"nSPDData", "sweighted data" , 100,0, 600}, "nSPDHits", "sig_sw"); 
    TH1D::SetDefaultSumw2();
    nSPDMC->SetLineColor(6);
    nSPDMC->SetMarkerColor(6);
    nSPDMCGB->SetLineColor(2);
    nSPDMCGB->SetMarkerColor(2);
    nSPDData->SetLineColor(4);
    nSPDData->SetMarkerColor(4);
    TCanvas* c1=new TCanvas("c1");
    nSPDMC->Scale(100./nSPDMC->Integral());
    nSPDMCGB->Scale(100./nSPDMCGB->Integral());
    nSPDData->Scale(100./nSPDData->Integral());
    nSPDMC->DrawClone("same e");
    nSPDMCGB->DrawClone("same e");
    nSPDData->DrawClone("same e");
    c1->BuildLegend();
    nSPDMC->SetTitle(";nSPDHits;Arbitaray Scale");

    /* c1->SetLogy(); */
}
